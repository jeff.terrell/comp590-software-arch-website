(ns user)

;; We walked through the Datomic Data Model here:
;; https://docs.datomic.com/cloud/whatis/data-model.html

;; Here are the things you need to know:
;;
;; - Datomic is a database system for storing data.
;; - Datomic is a database that exhibits an emphasis on (immutable) "values"
;;   over "places".
;; - A database value is a set of facts.
;; - Each fact, called a "datom", is atomic (i.e. the most granular thing you
;;   can store) and immutable.
;; - A database value is a point-in-time snapshot of the whole database, like a
;;   tree in git.
;; - A fact contains an entity E, an attribute A, a value V, and some
;;   bookkeeping stuff.
;; - An entity is a set of facts that are all about the same E value (entity
;;   ID).
;; - You can visualize an entity as a table.
;; - The transaction ID (4th item in a fact tuple) represents when a fact was
;;   added or retracted.
;; - The Op (5th item in a fact tuple) represents whether the fact was added or
;;   retracted.
;; - With transactions, Datomic automatically keeps an audit log of how and when
;;   data changes.
;; - You can visualize an entity at a point in time as a map.
;; - The schema is "universal", meaning that any entity can have any attribute.
;; - Indexes are managed by Datomic, and only 3-4 are needed to make every query
;;   fast. (This is way easier than relational databases, where you have to
;;   identify the need for indexes manually.)
;; - Datomic, like git, separates value from time, so, like git, it has good
;;   facilities for interacting with time.
;; - Datomic is an "accumulate only" data store, so data is never deleted
;;   (although facts can be retracted, and the retraction is also stored in the
;;   database).
;; - The following code and comments, which you can evaluate in a REPL that you
;;   start like so:
;;   clj -Sdeps '{:deps {datomic-client-memdb {:mvn/version "1.0.2"}}}'

(comment
  ;; create a database and connect to it
  (require '[compute.datomic-client-memdb.core :as memdb])
  (def client (memdb/client {}))
  (require '[datomic.client.api :as d])
  (d/create-database client {:db-name "demo"})
  ;; A connection is needed to "transact" new data.
  (def conn (d/connect client {:db-name "demo"}))

  ;; Schema information is just data. We define attributes. These attributes are
  ;; themselves entities. Things get a bit meta here.
  (def schema
    ;; This is the long-form way with E/A/V tuples.
    #_[[-42 :db/ident :artist/name]
       [-42 :db/unique :db.unique/identity]
       [-42 :db/cardinality :db.cardinality/one]
       [-42 :db/valueType :db.type/string]]

    ;; Or we can use the map form:
    [{:db/ident :artist/name  ; the identifying name of the attribute
      :db/unique :db.unique/identity  ; only one artist of a give name allowed
      :db/cardinality :db.cardinality/one  ; the value is singular, not plural
      ;; So, for the entity E represented by this map, the following line adds a
      ;; fact for entity E, attribute :db/valueType, and value :db.type/string.
      :db/valueType :db.type/string}
     {:db/ident :album/name
      :db/valueType :db.type/string
      :db/cardinality :db.cardinality/one}
     {:db/ident :album/artist
      :db/valueType :db.type/ref
      :db/cardinality :db.cardinality/one}
     {:db/ident :track/name
      :db/valueType :db.type/string
      :db/cardinality :db.cardinality/one}
     {:db/ident :track/album
      :db/valueType :db.type/ref
      :db/cardinality :db.cardinality/one}])

  ;; Let's transact the schema to add it to the database.
  (d/transact conn {:tx-data schema})
  ;; We get back :db-before and :db-after values.
  ;; These are database values: immutable point-in-time snapshots of the data,
  ;; much like a tree in git.

  ;; Let's transact some data.
  (def class-tracks
    [{:db/id "infected"  ; a temporary ID that we can use in this transaction
      :artist/name "Infected Mushroom"}
     {:db/id "im-aom"
      :album/name "Army of Mushrooms"
      :album/artist "infected"}
     {:track/name "Never Mind"  ; we don't need a temp ID here; nothing refers to it
      :track/album "im-aom"}
     {:db/id "foo-fighters"
      :artist/name "Foo Fighters"}
     {:db/id "ff-tcats"
      :album/name "The Colour and the Shape"
      :album/artist "foo-fighters"}
     {:track/name "Everlong"
      :track/album "ff-tcats"}])
  (d/transact conn {:tx-data class-tracks})
  (def db-with-schema (:db-before *1))

  ;; Let's get the latest database value from the connection.
  (def db (d/db conn))
  ;; And build a query for artist and track names:
  (def query
    ;; The ? things are variables. They're just Clojure symbols. We need to
    ;; quote this map to avoid the symbols being evaluated into values (which
    ;; would fail because no vars exist with these names).
    '{:find [?artist-name ?track-name]
      ;; Datomic's job is to _unify_ these variables; that is, to find something
      ;; for each variable that works with all of the clauses below. For
      ;; example, whatever is chosen for ?artist-entity needs to (1) be an
      ;; entity ID with an :artist/name attribute (from the first clause), and
      ;; (2) be the value of another fact having attribute of :album/artist
      ;; (from the second clause; recall that the :album/artist attribute has a
      ;; value type of :db.type/ref). Each of the clauses below is in the form
      ;; entity, attribute, value. This is logic programming!
      :where [[?artist-entity :artist/name ?artist-name]
              [?album-entity :album/artist ?artist-entity]
              [?track-entity :track/album ?album-entity]
              [?track-entity :track/name ?track-name]]})

  ;; Execute the query against database `db`.
  (d/q {:query query, :args [db]})

  ;; The result is a set of vectors, one for each result tuple, with each vector
  ;; having the values specified in the `:find` part of the query. In this case:
  ;; #{["Infected Mushroom" "Never Mind"]
  ;;   ["Foo Fighters" "Everlong"]}

  ;; Let's do the same query against the db value that doesn't have data:
  (d/q {:query query, :args [db-with-schema]})
  ;; Result: #{}, i.e. the empty set. (Makes sense, right?)

  ;; Let's add some more tracks, and define a helper function to make it easier.
  ;; Notice that the transaction data is just data, so we can use pure functions
  ;; to build up what we need.
  (defn track-datoms [[artist album track]]
    (let [artist-id (str "artist/" artist)
          album-id (str "album/" album)]
      [{:db/id artist-id, :artist/name artist}
       {:db/id album-id, :album/name album, :album/artist artist-id}
       {:track/name track, :track/album album-id}]))

  (def more-tracks
    (vec
      (mapcat track-datoms
              ;; artist              album                        track
              [["Infected Mushroom" "Vicious Delicious"          "Becoming Insane"]
               ["Shpongle"          "Tales of the Inexpressible" "Star Shpongled Banner"]
               ["Sir Sly"           "Don't You Worry, Honey"     "&Run"]
               ["Muse"              "Drones"                     "The Handler"]])))

  ;; Let's save the database value without this data for later use.
  (def db-with-some-data db)
  ;; Now transact the new data and get the latest database value.
  (d/transact conn {:tx-data more-tracks})
  (def db (d/db conn))

  (d/q {:query query, :args [db]})
  ;; Results:
  ;; #{["Infected Mushroom" "Becoming Insane"]
  ;;   ["Infected Mushroom" "Never Mind"]
  ;;   ["Foo Fighters" "Everlong"]
  ;;   ["Shpongle" "Star Shpongled Banner"]
  ;;   ["Muse" "The Handler"]
  ;;   ["Sir Sly" "&Run"]}

  ;; Note how we pass the database value in as an argument. We can pass a
  ;; different db in and get a different result.
  (d/q {:query query, :args [db-with-some-data]})
  ;; #{["Infected Mushroom" "Never Mind"]
  ;;   ["Foo Fighters" "Everlong"]}
  ;; (Same as before:)

  ;; We can also add parameters to the query.
  (def parameterized-query
    '{:find [?album-name ?track-name]
      ;; Here we say `$`, which in Datomic means "the database value", and then
      ;; use a new variable name, which unifies with the where clauses below.
      :in [$ ?artist-name]
      :where [[?artist-entity :artist/name ?artist-name]
              [?album-entity :album/artist ?artist-entity]
              [?album-entity :album/name ?album-name]
              [?track-entity :track/album ?album-entity]
              [?track-entity :track/name ?track-name]]})

  ;; Let's make a function to run the query:
  (defn album-and-track-tuples-for-artist [db artist]
    (d/q {:query parameterized-query
          :args [db artist]}))

  (album-and-track-tuples-for-artist db "Infected Mushroom")
  ;; => #{["Vicious Delicious" "Becoming Insane"]
  ;;      ["Army of Mushrooms" "Never Mind"]}
  (album-and-track-tuples-for-artist db-with-some-data "Infected Mushroom")
  ;; => #{["Army of Mushrooms" "Never Mind"]}
  (album-and-track-tuples-for-artist db "Muse")
  ;; => #{["Drones" "The Handler"]}

  ;; fin
  )
