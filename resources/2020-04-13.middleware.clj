;; To run this code, put it in a `src/` subdirectory, and add a `deps.edn` file
;; with the following line:
;; {:deps {ring {:mvn/version "1.8.0"}}}
;; Then run `clojure -m server` to start the server, and use a tool like
;; httpie [1], curl [2], or your browser's dev tools [3] to send requests to
;; the server and see the response.
;;
;; [1] https://github.com/jakubroztocil/httpie
;; [2] https://curl.haxx.se/
;; [3] https://developer.mozilla.org/en-US/docs/Learn/Common_questions/What_are_browser_developer_tools

(ns server
  (:require [ring.adapter.jetty :refer [run-jetty]]))

(defn list-commits []
  ["commit1" "commit2"])

(defn list-branches [world]
  (:branch-list world))

(defn handler [request]
  (prn 'handler)
  (let [{:keys [request-method uri world]} request
        method|path (str (.toUpperCase (name request-method)) uri)]
    (case method|path
      "GET/branches" (list-branches world)
      "GET/log" (list-commits)
      {:status 404})))

(defn wrap-catch [handler]
  (fn [request]
    (try
      (prn 'wrap-catch 'input)
      (let [response (handler request)]
        (prn 'wrap-catch 'output)
        response)
      (catch Throwable e
        {:status 500
         :headers {"Content-Type" "application/edn"}
         :body (pr-str e)}))))

(defn wrap-edn [handler]
  (fn [request]
    (prn 'wrap-edn 'input)
    (let [response (handler request)]
      (prn 'wrap-edn 'output)
      (if-not (vector? response)
        response
        {:status 200
         :headers {"Content-Type" "application/edn"}
         :body (pr-str response)}))))

(defn wrap-world [handler world]
  (fn [request]
    (prn 'wrap-world 'input)
    (let [request (assoc request :world world)
          response (handler request)]
      (prn 'wrap-world 'output)
      response)))

;; Order of operations:
;;
;; 1. input - wrap-world
;; 2. input - wrap-edn
;; 3. input - wrap-catch
;; 4. handler
;; 5. output - wrap-catch
;; 6. output - wrap-edn
;; 7. output - wrap-world

(defn -main [& _]
  (let [branch-list ["master" "feature-branch-1"]
        world {:branch-list branch-list}]
    (run-jetty (-> handler
                   wrap-catch
                   wrap-edn
                   (wrap-world world))
               {:port 3005})))
