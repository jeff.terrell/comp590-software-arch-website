# COMP 590 Website

Content for UNC's COMP 590 (software architecture) web site, as taught by Prof.
Jeff Terrell in spring 2020.

## Usage

### Build the site

To build the site based on the current code, which takes about 2 seconds:

```
clojure -A:ssg
```

The built site is ready in the `dist/` directory with the following structure:

- `/blog/index.html` is a page containing all post summaries, latest first. At
  this point, no pagination is performed.
- Each page is compiled and becomes a top-level directory in the site. For
  example, a page in `pages/about.md` produces `dist/about/index.html`.
- Each post is compiled and becomes a post in a date-based subdirectory under
  `posts/`. For example, a post with certain metadata will produce
  `dist/2018/12/10/slugified-title/index.html`.

### Add content

There are several sources of content:

- The header and footer templates in the `template/` directory. These are used
  verbatim on every page, except that the title of each page is substituted into
  the header template.
- The site style rules in `resources/style.css`.
- The script and style files and other resources in the `resources/` directory.
  If you add other files to this directory, they will not be copied
  automatically (although that wouldn't be a bad feature to add).
- The pages in the `pages/` directory. These are markdown files with YAML
  metadata (which is required). The metadata must include a `title` property.
  Note that the title will be included in the output automatically, so don't
  re-state the title in the markdown.
- The posts in the `posts/` directory. These are like pages, but with extra
  metadata required.

To add a post (or page), I recommend copying an existing post (or page) to get
the metadata formatting right, then replace the metadata and content as
necessary.

### Develop Locally

You can [run a local web
server](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/set_up_a_local_testing_server)
to see everything in your browser.

In order to run the local web server and render the website's content, navigate
to the `/dist/` directory, then follow the instructions at the link above or as
shown below.

If using Python 3:

```
python http.server
```

If using Python 2:

```
python -m SimpleHTTPServer
```

Your website should be available at localhost:8000.

## License

Copyright &copy; 2020, Jeff Terrell

Distributed under the MIT License.
