---
title: Piazza
date: 2020-01-10
---

We'll be using Piazza this semester for Q&A. More details follow.

<!--more-->

You can [join the community
here](https://piazza.com/unc/spring2020/comp590145/home). Please prefer this
medium over email for questions so that other students can benefit from the
question and answer.

Please keep the [Honor Code](../syllabus/#honor-code) and collaboration policy
in mind regarding what you ask there.
