---
title: Notes from Jan. 27 lecture
stem: notes-jan-27
---

### Announcements

- Music: _Polyphonic Song: Edi beo thu hevene quene_ by Anonymous 4, on the album An English Ladymass: Medieval Chant and Polyphony.
    - This is an example of medieval chant.
    - We'll be talking about abstraction today, and I figure this is fairly
      abstract music.
- Feedback for me? Use the form posted to the course home page.
- Need to request an absence for a midterm exam? Use the form posted to the
  course home page. Remember: at most 1 absence allowed per person.
- Some people have successfully completed Assignment 1 with full credit, so we
  now know it's possible. (If you didn't get an email from Gradescope, let me
  know.) We did identify a lack of specificity about what to submit exactly,
  which I clarified in the assignment submission instructions.
- The [Clojure Cheatsheet](https://clojure.org/api/cheatsheet) is a nice thing
  to have up while coding.

### Warm-up exercise

> Define a Clojure function named `double-items`, that takes a sequence of
> numbers and returns a sequence of the same numbers doubled.

Solution:

```clojure
(defn double-items [xs]
  (map #(* 2 %) xs))
```

You'll definitely need to know how to use `defn` in your assignments.

### Clem, the Clojure Error Mediator

Here are [instructions for setting up Clem in
Cursive](https://applab.unc.edu/posts/2020/01/17/integrating-clem-cursive/).

In a Clem-enabled REPL, if an exception occurs, your exception will be sent to
Clem's online database of Clojure exceptions and explanations. If an explanation
exists for your exception, it will be printed in your REPL. If not, you will
have the opportunity to write an explanation yourself.

### Cursive tips

If you are annoyed that some edits you want to make to Clojure code are
prevented, you can disable that feature under edit -> structural editing ->
toggle structural editing. (You can also do this globally at settings -> editor
-> general -> smart keys -> use structural editing.) Cursive is attempting to
keep your parens balanced. This is good if you know structural editing
shortcuts, but if not, it can get in your way. You can [read more about
structural editing in Cursive
here](https://cursive-ide.com/userguide/paredit.html).

It may also be helpful to let Cursive indent your code for you. There's [a
pretty consistent style](https://guide.clojure.style/) that people use for
Clojure code. It's probably a little different than languages you're used to,
but it can be quite readable once you get used to it. In Cursive, use code ->
reformat code (and similar options) to do this. There's more info
[here](https://cursive-ide.com/userguide/formatting.html).

### Clojure debugging

The best tool you have in debugging your code is not a debugger, it's your mind.
Debuggers can certainly be useful, but they're no substitute for actually
understanding what's happening.

This is a rich topic that I want to get into more later, but for now, let's
just cover the basics.

To debug in Clojure, _isolate_ your bug by iterating smaller in two dimensions:

- your code
- your data

In other words, find the smallest or simplest data structure that triggers the
error, and likewise find the smallest bit of code that triggers the error. [A
prominent Clojure programmer](https://github.com/stuarthalloway) has named this
exercise "[running with
scissors](https://github.com/stuarthalloway/presentations/wiki/Running-with-Scissors)".

In order to iterate, you need (a) a reproducible error case and (b) a means of
triggering it. The REPL is a great tool for (b), especially when combined with
an editor. Put code that triggers the error in a `(comment ,,,)` block, along
with any needed data definitions or require statements. Then start paring it
down along the two dimensions.

When you have a reproducible and somewhat isolated bug, so-called "printf
debugging" is great! It gives you visibility about what's actually happening.
Because you've isolated it, you're not getting a bunch of distracting noise.

For example, consider this code to compute a sample standard deviation:

```clojure
(defn sum [xs]
  (reduce + 0 xs))

(defn mean [xs]
  (/ (sum xs)
     (count xs)))

(defn square [x]
  (* x x))

(defn stdev [xs]
  (let [this-mean (mean xs)
        errors (map #(- % this-mean 1.0) xs)
        squared-errors (map square errors)
        sq-err-sum (sum squared-errors)]
    (Math/sqrt (/ sq-err-sum (dec (count xs))))))
```

We're told that the correct answer for the following input:

    (stdev [10 12 23 23 16 23 21 16])

...is `5.2372293656638` but instead we're getting an answer of
`5.3452248382484875`. What's going on?

Let's get a REPL going with the test case. Then pare down the data to `[2 4 6]`,
which should return `2` but instead returns `2.345207879911715`. Does `sum` give
the right answer? Yes. Does `mean`? Yes. Does `square`? Yes. So the bug is in
`stdev` somewhere.

Let's break down the operation of `stdev` into steps to isolate the bug further.
The first `let` binding just calls `mean`, which we checked already. What does
the second binding do? Computes something called errors. Does the result of
`(-3.0 -1.0 1.0)` look right? Hmm, maybe not. It's oddly asymmetrical. What's
going on? Ah, there's a `1.0` being subtracted from each error. Sure enough,
that's not in the formula for stdev. Let's take it out. Now `stdev` gives the
right answer, for both the reduced input and the full input.

Another tip for debugging: `prn` prints things in a Clojure-readable form, which
is nice because e.g. you can distinguish between a string and a symbol. I like
to tag my `prn` statements with a symbol describing what I'm printing. For
example, if I'm checking what the value of the `args` var is, I'd say `(prn
'args args)`.

In the context of the autograder, all output gets captured, which would include
your printing statements. To get around this, you can print to the standard
error stream instead of the standard output stream, like so:

```clojure
(binding [*out* *err*]
  (prn 'args args))
```

### Abstraction basics

What is the essence of good software design? What is the key activity in making
a program _good_?

> The essence of good software design is eliminating incidental complexity.

Complexity is what makes programming hard. The key observation here, from Dr.
Brooks in [No Silver
Bullet](http://worrydream.com/refs/Brooks-NoSilverBullet.pdf), is that not all
complexity is essential to the problem at hand. Much of it is smuggled in by:

- our approach
- our language
- our tools
- our laziness

As a simple example, consider the standard `for` loop:

```
for (int i = 0; i < arr.length; i++) {
  arr[i] *= 2;
}
```

Lots of incidental complexity! We have to say:

- What type of variable to use
- What variable name to use
- Where to start counting
- When to stop counting
- What to do after each iteration
- What to do at each iteration

Only the last thing is essential! Consider the equivalent (though non-mutative)
Clojure code:

```
(map #(* 2 %) arr)
```

Lesson: incidental complexity is difficult to spot because
_we don't think about it_. We actively try _not_ to think about it.

Languages differ not in what's possible to say, but in what you have to say to
express a thought. Example: in English, if I'm talking about some number of
paperclips, I have to say something about the quantity. Maybe "a paperclip",
"some paperclips", "a few", "a couple", "an unknown number", etc. At least I
have to say "paperclip" or "paperclips", distinguishing between one and
multiple. I can't concisely express the idea of paperclips without specifying
quantity. If quantity isn't relevant, then English is forcing me to have some
incidental complexity.

In Spanish, I have to spend some words and syllables on the gender of nouns and
making sure all the genders of words agree in all the right places. I don't have
to do that in English, because e.g. "paperclip" carries no information about
gender.

Key idea of incidental complexity: it depends what you want to do. Complexity
that is incidental for one purpose might be essential for another. It depends.

Getting back to abstraction. Abstract, as a verb, comes from the Latin roots
"ab", meaning "from" or "away", and "trahere", meaning "to pull" or "to draw".
In other words, "to draw away".
([Source](https://www.merriam-webster.com/dictionary/abstract).) A good word in
English that conveys this idea more clearly is "extract".

So in our for vs. map example above, map draws away various considerations that
we don't need to care about. It extracts them from what you have to say. Those
details aren't visible in the code.

This is good, _if and only if_ the drawn away details are incidental rather than
essential. Again, it depends. If they were essential, now we have to look in two
places to get all the essence in our minds.

So the goal of good software design is to remove the incidental complexity, so
that your code looks more like this:

![](https://blog.ploeh.dk/content/binary/essential-accidental-complexity-shells-brooks-scenario.png)

or even this:

![](https://blog.ploeh.dk/content/binary/essential-almost-no-accidental-complexity-shells.png)

rather than this:

![](https://blog.ploeh.dk/content/binary/accidental-complexity-with-tiny-core-of-essential-complexity.png)

(Images from [here](https://blog.ploeh.dk/2019/07/01/yes-silver-bullet/).)

### More on Clojure: 16. threading macros

```clojure
(-> 7 inc inc inc)        ; => 10 (the "thread first" macro)
(-> 7 inc (/ 2) (+ 4))    ; => 8 (each result is 1st arg of next form)
(->> 3 dec (- 8))         ; => 6 (the "thread last" macro)
;; sum of the first 100 non-neg ints divisible by 3
(->> (range)           ; (notice laziness)
     (filter #(zero? (rem % 3)))
     (take 100)
     (reduce + 0))
;; exercise: sum of the first 10 odd squares
(->> (range)
     (map #(* % %))
     (filter odd?)
     (take 10)
     (reduce + 0))
(as-> {:a 1, :b 2} m      ; as-> lets you control...
  (update m :a + 10)      ; ...where the result goes...
  (if update-b?           ; ...in the next form.
    (update m :b + 10)
    m))
```

