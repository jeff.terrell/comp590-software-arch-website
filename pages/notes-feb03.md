---
title: Notes from Feb 3 lecture
stem: notes-feb-03
---

### update on the test

-   I've written the questions.
-   Main thing I'll say is that the notes on the web site are a good
    study resource. Some in-class exercises are similar to questions
    on the test.
-   Cheat sheet allowed. 1 page, double-sided, printed or hand
    written.
-   Hard for me to calibrate how hard to make the test. If it ends
    up being too hard, that's easier to adjust for via curving
    rather than adjusting after making it too easy. (I won't curve
    your grades down, and might not curve at all.)
-   gonna try hard to have all grades assigned and posted by the
    following class

### summarize feedback from take-away questions last time

-   having slides would be helpful, or at least posting notes before
    class
-   clarification: what is a higher-order function?
-   clarification: when I say complexity, I don't mean algorithmic
    or runtime complexity
-   what's a monad? it's not important. point is that it means
    something specific but most people (incl. programmers) don't
    know what it is, and that that doesn't disqualify it from being
    a good name
-   confusion about cohesion (and examples might be helpful)
-   examples of good cohesion (from SSG repo)
    -   cd \~/code/unc/ssg/src/edu/unc/applab/ssg; vim -p \*
-   examples of poor cohesion (from matchmaker repo)
    -   cd \~/code/unc/matchmaker; git show f0a13:src/user.clj |
        vim -
-   example of hierarchical modules in Clojure code and how it looks
    in Cursive
    -   cd \~/code/toolsmiths/recharge/src/clj/recharge; tree
    -   head -n1 \*.clj
    -   head -n1 parse/\*.clj

### followup on names:

-   the amount of meaning that a person can connect to via words is
    not static
-   e.g. this class will be successful if it connects you to more
    meaning
-   so say you're using the name "monad" in your code, but you know
    most readers won't know what that is, what do you do? maybe
    connect them to a resource so that they can learn what a monad
    is and connect the word to meaning
-   or say you come up with an original name that you feel captures
    the meaning, but it's not obvious to a reader what the meaning
    is. What then? Write an explanation! Those kind of comments are
    super valuable.
-   example: top-level docstrings for a namespace, e.g. in SSG repo

### thinking in Clojure

#### be aware of lazy semantics

-   many sequence functions, including `map`, `for`, and
    `filter`, are **lazy**
-   they don't actually do stuff until something asks for
    results
-   normally this isn't a problem: it doesn't affect the
    returned results
-   but it can be confusing when you're expecting effects to
    happen
-   example:

```clojure
(def s (map #(do (prn %) %) (range)))
(do
  (println "the first value of s is:" (first s))
  (println "the second value of s is:" (second s))
  (println "the third value of s is:" (nth s 2)))
```

-   (this can get even more confusing because some sequence functions
    read a chunk of like 16 values at a time, so effects are clumped
    together in surprising ways)
-   the lesson: be aware that sequence-related things don't necessarily
    happen right away in Clojure
-   if you want to force it to happen right away:
    -   use `mapv` instead of `map` (returns a vector, so can't be lazy)
    -   wrap something lazy in a call to `doall`
-   note also: the REPL realizes a lazy sequence when it prints the
    returned value, so you might have laziness and not realize it when
    you're in a REPL

#### don't throw away values

-   if you have multiple expressions in a fn body or do block, be
    aware that the first return value is thrown away
-   so return value of anything but last expression should be
    irrelevant--such exprs should only be important for their side
    effects
-   example:

```clojure
(do
  (Integer/parseInt arg)
  (pascal arg))
```

-   this can happen in a do block, let block (where you can have
    multiple expressions in the body), function definition with defn or
    fn, etc.
-   when does it make sense to do this? when you are calling a function
    for its effect rather than its return value
-   example:

```clojure
(do
  (prn 'parseInt (Integer/parseInt arg))
  (Integer/parseInt arg))
```

-   we don't care about what `prn` returns, only what it does, so this
    is fine

#### don't `def` except at the top level of a namespace

-   vars get defined with a scope of the namespace
-   this can cause weird bugs if you are defining things inside a
    function
-   if you define at the top-level only, you avoid surprises:
    namespace scope is what you expect if you do that
-   inside a function, use `let` to have a limited scope for a
    binding
-   the scope of a let binding is only:
    -   the values of later bindings
    -   the body of the `let` (i.e. after the bindings but before
        the end of the `let`)
-   but I want to assign a new value to a variable! how do I do
    that? I'll get there; stay tuned.

#### but I'm used to for loops!

-   if you're looping for each item in a sequence, use `map` instead
-   if you want an `if` inside the body of the loop, use `filter`
    instead
-   if you're trying to build up some summary value based on each
    item in the sequence, use `reduce` instead (more on this in a
    bit)

#### but I'm used to objects!

-   think about data structures instead
-   need named fields? Use a hashmap.
-   don't worry about privacy and encapsulation: everything is
    immutable, which solves the same problem OO encapsulation
    solves, and in a much simpler way
-   need a method? pass your hashmap as an argument to a function
-   less ceremony, so less incidental complexity
-   just remember that the hashmap is immutable

#### but how do I get things done if I can't change anything??

-   **this is perhaps the most important question you could ask when learning
    Clojure**
-   (sorry I haven't covered it until now! I'm learning too.)
-   what good is it to name a value, if that value can never change?
-   example: I want to initialize `sum` to `0`, then update it for each item in
    my sequence of numbers
-   bad code, but which still gets the job done:

```clojure
(do
  (def sum 0)
  (doall  ; what would happen if I forgot this??
    (for [x (range 7 11)]
      (def sum (+ sum x))))
  (prn 'sum sum))
```

-   but we learned that `def` outside the top level is no good! What to
    do?
-   you might recall that a way to sum numbers is with `reduce`. Let's
    take a closer look at what's happening:

```clojure
(reduce + 0 (range 7 11))
(reduce +                                              0    '(7 8 9 10))
(reduce (fn [sum-so-far item] (+ sum-so-far item))     0    '(7 8 9 10))
;; call the fn: sum-so-far is 0 and item is 7
(reduce (fn [sum-so-far item] (+ sum-so-far item)) (+  0 7) '(8 9 10))
(reduce (fn [sum-so-far item] (+ sum-so-far item))     7    '(8 9 10))
;; now call the fn: sum-so-far is 7 and item is 8
(reduce (fn [sum-so-far item] (+ sum-so-far item)) (+  7 8) '(9 10))
(reduce (fn [sum-so-far item] (+ sum-so-far item))    15    '(9 10))
;; now call the fn: sum-so-far is 15 and item is 9
(reduce (fn [sum-so-far item] (+ sum-so-far item)) (+ 15 9) '(10))
(reduce (fn [sum-so-far item] (+ sum-so-far item))    24    '(10))
;; now call the fn: sum-so-far is 24 and item is 10
(reduce (fn [sum-so-far item] (+ sum-so-far item)) (+ 24 10) '())
(reduce (fn [sum-so-far item] (+ sum-so-far item))    34     '())
;; base case of reduce, an empty list: return accumulated value
34
```

-   it looks like we're mutating `sum-so-far`!
-   but we're not actually; we're just calling the function with
    different arguments
-   and actually, `reduce` is doing that for us. We're just telling
    `reduce` what to pass to the function at each step.
-   is this just sleight of hand? yes, in that we're just moving the
    operation elsewhere. no, in that there's actually some key
    differences to doing things this way, which we'll cover as the
    semester goes on
-   remember: `reduce` is useful whenever you want to build up some
    summary value, built up somehow from each item in a sequence. You
    tell it the "somehow" (i.e. the function), the initial value of the
    summary, and the sequence, and it does the work.
-   use `reduce` when you can, but if `reduce` doesn't fit, there's a
    more general purpose construct in Clojure: `loop` and `recur`. Let's
    use it to compute the same sum:

```clojure
(loop [sum-so-far 0
       remaining-items (range 7 11)]
  (if (empty? remaining-items)
    sum-so-far
    (let [[item & rest] remaining-items]
      (recur (+ sum-so-far item)
             rest))))
```

-   what's happening here? we start a loop with initial values, then
    `recur` with different values.
-   the call to `recur` is effectively starting the loop over with
    different values bound to the same names
-   note that the call to `recur` has to happen in the "tail" position,
    i.e. as the very last thing that happens in the function
-   so e.g. this factorial function wouldn't work:

```clojure
(loop [n 5]
  (if (zero? n)
    1
    (* n (recur (dec n)))))
```

-   ...so we have to rearrange it to carry the result-so-far as one of
    the loop parameters, so that we can save the `recur` for the tail
    position:

```clojure
(loop [n 5
       product-so-far 1]
  (if (zero? n)
    product-so-far
    (recur (dec n)
           (* n product-so-far))))
```

-   all that said, when possible, use a function to do the
    transformation for you, e.g. `map`, `reduce`, or other things
    mentioned on the Clojure cheatsheet.

