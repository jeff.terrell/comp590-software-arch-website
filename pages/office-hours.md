---
title: Office Hours
stem: office-hours
---

Please see our availability for office hours below. Office hours will be held
in SN 141. You can [see a map
here](https://cs.unc.edu/about/buildings/floor-plans/).

<div>
<iframe src="https://calendar.google.com/calendar/embed?src=cs.unc.edu_0546hppiqga9ft76jc7fstk5ns%40group.calendar.google.com&ctz=America%2FDetroit" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
</div>

