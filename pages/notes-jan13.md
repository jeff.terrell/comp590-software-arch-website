---
title: Notes from Jan. 13 lecture
stem: notes-jan-13
---

### Announcements

- Intro music was _Polynomial C_ by Aphex Twin: classic 90s electronic music
  with good structure and layers, like we'll be composing in our programs.
- (From the registrar:) remember to acknowledge that you've started academic
  activity this semester.
- Lecture notes from last class are online.
- Introducing LAs (or at least 2 of them, Shaik and Vraj)

### Quiz 1: Clojure data structure warm-up

> Pair up with somebody near you. Capture in a single data structure a name
(e.g. first name, screen name) and origin (i.e. where you're from) for each of
you. The data structure should be nested and allow looking up a person by an ID
that you assign.

```clojure
;; Solution 1: keyword IDs, hashmaps in a hashmap
{:jeff {:name "Jeff", :origin "Texas"}
 :henry {:name "Henry", :origin "North Carolina"}}

;; Solution 2: index IDs, hashmaps in a vector
[{:name "Jeff", :origin "Texas"}
 {:name "Henry", :origin "North Carolina"}]
```

### How to attend this lecture

- I have loads of new content here
- I don't expect you to get it all first pass
- It'd be like throwing 100 new foreign-language vocabulary words at you in an hour: it won't all stick
- Goal for this lecture: understand what's going on
- Later:
    - Do quizzes in class (graded for participation, not correct answer)
    - Do assignment
    - Maybe make yourself some spaced-repetition flashcards (see Anki program on Mac)
    - (A learning principle: effort at recall makes learning stick)
    - Use references as needed, incl.
        - My notes from this lecture
        - Built-in help in REPL or editor (see #6 below)
        - Clojure basics cheatsheet (by LA Shaik): see course website (soon)
        - [Clojure cheatsheet](https://clojure.org/api/cheatsheet)
        - [Clojure language reference](https://clojure.org/reference)

### Cursive editor setup

- With [Cursive installed](https://cursive-ide.com/userguide/index.html), start
  a new "Deps" project.
- In the upper right, click the "add configuration" button.
- Click the "+" to add a new configuration, and select the Clojure REPL ->
  Local option.
- Select the "deps" option. (If it doesn't exist, see Piazza.)
- Name the configuration REPL.
- Click OK to create the configuration.
- Click the green "play" button in the upper right to start the REPL.
- In the project structure view, right-click on "Scratches and Consoles" and
  select "new scratch file", then choose the Clojure option.
- To send an expression to the REPL from your scratch file, select Tools ->
  REPL -> Send ',,,' to REPL, or on MacOS do command-shift-P.

### 1. Conditionals

```clojure
(= 2 2)
(= 2 2.0)
(not= 2 2)
<, <=, >=, >                 ; other comparison functions
(= [1 2 [3 4]] [1 2 [3 4]])  ; true (= works with collections)
(= #{1 2 3} #{3 2 1})        ; true (order unimportant for sets)
(= {:foo 1, :bar 2}
   {:bar 2, :foo 1})         ; true (order unimportant for hashmaps)
(if true "true" "false")     ; conditional
```

### 2. Functions

```clojure
#(+ 5 %)          ; function literal: add 5 to the given argument
(fn [x] (+ 5 x))  ; anonymous function; equivalent but not comparable with =
(#(+ 5 %) 7)      ; call your function
```

- I showed a simple example of "structural editing" (under the "edit" menu in
  Cursive): wrapping the given form in parentheses (using command-shift-9 in
  MacOS).
- Structural editing makes use of how Clojure syntax works: the code itself
  provides the syntax tree, because each evaluation step is explicitly defined
  by parentheses. The parens might seem superfluous, but their required
  presence makes powerful things like structural editing possible.
- Get good at these shortcuts and you will feel like a wizard. :-)

### 3. `let` blocks

```clojure
(let [x 5] x)     ; 5
(let [x 5
      y 4]
  (+ x y))        ; 9
(let [x 5
      y (dec x)]
  (+ x y))        ; 9 (can refer to previous bindings)
(let [x 5
      x (dec x)]
  (dec x))        ; 3 (can overwrite bindings)
(let [fn inc]
  (fn 7))         ; 8 (can use almost any name for a binding)
```

### 4. vars

```clojure
(def x 5)         ; bind var #'x to value 5
(var x)           ; the var itself (a "reference type")
#'x               ; syntax shorthand
x                 ; 5 (x is a "symbol")
'x                ; the symbol x, quoted to prevent evaluation
(quote x)         ; long-form version
(def x 4)         ; can redefine vars
```

- In Clojure, var is a _reference type_: a var _points to_ an immutable value.
- The var can change (by calling `def` again), but the value itself never
  changes.
- For example, in the above code, 5 is still 5 after the redefinition. If the
  value had instead been `[1 2 3]`, that vector would still be itself even if
  the var that pointed to it changed.

### Quiz: functions

> Pair up with somebody near you. Write a function to square a given number, and
apply the function to the number 16.

```clojure
;; Solution 1: function expression
((fn [x] (* x x)) 16)

;; Solution 2: function literal
(#(* % %) 16)
```

### 5. Vector operations

```clojure
(def v [7 8 9])
v                    ; [7 8 9]
(count v)            ; 3
(vector 7 8 9)       ; same
(get v 1)            ; 8 (index lookup)
(conj v 10)          ; [7 8 9 10] (conjoin)
v                    ; [7 8 9] (v was unchanged)
(assoc v 0 (dec 6))  ; [6 8 9] (overwrite an item with a new value)
(update v 0 dec)     ; [6 8 9] (transform an item by a function)
(vec '(1 3 5))       ; [1 3 5] (make vector from sequence)
```

- None of these functions actually changes the vector; rather, they return a
  transformed copy.
- Hint: use F1 on a function to see its docstring and examples.

### 6. REPL usage

- The following things work in a REPL environment only.

```clojure
v
(vec [7 8 9])
(= *1 *2)           ; previous 2 values
(doc conj)          ; (also try F1 in Cursive)
(apropos "str")     ; search for functions with given str in name
(find-doc "string") ; search docstrings for given string
(source conj)       ; display source of a function
*e                  ; most recent exception
(pst)               ; print stack trace of most recent exception
```

### 7. strings and printing

```clojure
(str :anything)     ; get string representation of a thing
(str "foo" "bar")   ; "foobar" (concatenate things)
(format "He said '%s'" "boo")  ; printf-style interpolation
(println "hello")
(prn 'hello)        ; prints things in a Clojure-readable way
(prn "hello")       ; prints "hello", _including quotes_
(prn 'v v)          ; e.g. for debugging
(pr-str v)          ; "[7 8 9]"
(pr-str "str")      ; "\"str\"" (including quotes)
```

