---
title: Watchlist for March 23 lecture
stem: watchlist-mar-23
---

### Please watch the following videos _before class_ on Monday, March 23

- [Moving to online classes (a.k.a. using Zoom for class)](https://youtu.be/sTUeZPL38o4) (16m)
- [git branches, refs, and the HEAD ref (a.k.a. concepts for A4)](https://youtu.be/E4FQOb6XUis) (29m)

You may also want to watch these videos, although I won't cover them in class,
and you won't be tested on them. They're just bonus videos, with the goal of
being helpful to you.

- [Using virtual office hours](https://youtu.be/GC3fGk051qc)
- [Pair programming with zoom tutorial](https://youtu.be/L-THvCKl2DA)
