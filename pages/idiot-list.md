---
title: Assignment 5: Idiot List
stem: idiot-list
---

### Due

Assignment 5 is due the last second of ~~Monday, April 13<sup>th</sup>~~
Tuesday, April 14<sup>th</sup>.  Late submissions reach their final penalty of
10% after the last second of ~~Monday, April 20<sup>th</sup>~~ Tuesday, April
21<sup>st</sup>, but are accepted even after that (see the
[syllabus](../syllabus/#assignments) for late policy).


### Bonus opportunity

If your latest submission is at or before the last second of
~~Friday, April 10<sup>th</sup>~~ Saturday, April 11<sup>th</sup>, you will
receive a 5% bonus to the autograded grade.


### Context

In the last 3 assignments, we implemented all of the Idiot database-manipulation
commands that we plan on doing this semester. Now let's add some features to
make traversing the history in the database easier.


#### Abbreviated addresses

As a convenience, git lets you specify as little as 4 characters to identify an
address. This works anywhere you give git an address. So, for example, where you
might say:

    git cat-file -p 21ed38c7a5054a8d09ccee89b64873f636ddbe00

&hellip;you can instead say:

    git cat-file -p 21ed


#### git rev-list

One of the &ldquo;plumbing&rdquo; (i.e. low-level) commands that git provides
(and uses) is `rev-list`, which traverses the chain of commit objects by
following the first parent of each commit. Here's an example command and output:

```
$ git rev-list @
21ed38c7a5054a8d09ccee89b64873f636ddbe00
f910b71b1a75c2b2436984696f266532dd522d31
3cb09a90adf32f507c815aa8f089426b32ce0711
ef9927244bd31fa5e28699c942df74ea9cc9e0d6
a55e25aec28531affd0bef26be038f14a8da992b
```

(The command is the line that starts with `$ `, here and below.) This output
indicates that `HEAD` (or `@`) points (perhaps indirectly, via a ref) to commit
`21ed`, whose first parent is commit `f910`, whose first parent is commit
`3cb0`, whose first parent is commit `ef99`, whose first parent is `a55e`. This
is much more convenient than sending each of those commits to `cat-file -p` in
turn!

The list will continue until it reaches a commit with no parents (which is
presumably the initial commit), or until the number of commits specified by the
`-n` switch has been printed. For example:

```
$ git rev-list -n 3 @
21ed38c7a5054a8d09ccee89b64873f636ddbe00
f910b71b1a75c2b2436984696f266532dd522d31
3cb09a90adf32f507c815aa8f089426b32ce0711
```

You can also specify a ref name to start the list from that commit instead. For
example, if the currently checked out branch was two commits ahead of master,
then we'd get this output:

```
$ git rev-list master
3cb09a90adf32f507c815aa8f089426b32ce0711
ef9927244bd31fa5e28699c942df74ea9cc9e0d6
a55e25aec28531affd0bef26be038f14a8da992b
```

By the way, the real [git rev-list](https://git-scm.com/docs/git-rev-list) is
pretty complicated!


#### git log

Git's log command is pretty well known. It behaves much like the lower-level
rev-list command above, but prints more than just the address of each commit.
Here's an example, with the same history as above:

```
$ git log --oneline
21ed38c Consolidate existing behaviors in e2e suite
f910b71 Use a map instead of a vector for dir contents
3cb09a9 Read the world as the first step
ef99272 Fix lints
a55e25a Solve A4
```

The real `log` supports all the options as `rev-list` and more. Its default
operation, i.e. without the `--oneline` argument, prints most of the same
information as `cat-file -p` does for commits. (Here's [the manual
page](https://git-scm.com/docs/git-log), if you're interested.) But we're only
going to implement the `--oneline` style of output. Idiot's `log` command will
need to accept the same arguments as `rev-list`: an optional `-n` switch and an
optional reference (both of which we'll constrain to come after the required
`--oneline` switch).


#### Clojure deps.edn

In the previous assignments, we've mostly glossed over that little `deps.edn`
file. Now we need it. It's what specifies what dependencies your Clojure program
needs. Like other languages, we can use third-party libraries that others have
written in our programs, and this is how. Here's a simple example that depends
on the `ring` library (more on that below).

```clojure
$ cat deps.edn
{:deps {ring {:mvn/version "1.8.0"}}}
```

Breaking that example down:

- Recognize the syntax? That's [edn, the extensible data
  notation](https://github.com/edn-format/edn). Just like
  [JSON](https://www.json.org/json-en.html) is a subset of Javascript's syntax
  intended for data exchange, so edn is a subset of Clojure's syntax. If you,
  like me, have grown spoiled by not having to type commas, you might like it.
- The top-level value is a hashmap, just like in Clojure.
- The only key so far is `:deps`, which lists the dependencies needed by the
  project.
- The dependencies are specified using a hashmap, from library name to
  coordinate. Libraries generally tell you what name they can be found with; the
  underlying technology is [Maven](https://maven.apache.org/what-is-maven.html),
  a popular Java dependency engine.
- Coordinates tend to be Maven versions (the `:mvn/version` above). In this
  example, we're depending on version 1.8.0 of ring, [as listed
  here](https://clojars.org/ring). (The `1.8.0` needs to be a string because
  it's not a valid Clojure/edn number.) Interestingly, coordinates can also
  point directly to a git commit e.g. on GitHub, so that library authors don't
  even need to release a versioned artifact. In fact, you can even use a local
  coordinate, e.g. if you wanted to clone a library locally and iterate on some
  changes before committing them and opening a pull request.
- This file is what the `clojure` and `clj` CLI tools look for dependencies and
  other configuration information in.

If you're interested, you can learn more about deps.edn and the Clojure CLI
tools in [the guide](https://clojure.org/guides/deps_and_cli) or the longer
[reference](https://clojure.org/reference/deps_and_cli).

As a bonus tip, you can specify aliases in your `deps.edn` file. Here's a
deps.edn file that includes an alias I use for testing:

```clojure
{:deps {ring {:mvn/version "1.8.0"}}
 :aliases
 {:test {:extra-deps {speclj {:git/url "https://github.com/kyptin/speclj"
                              :sha "a843b64cc5a015b8484627eff6e84bbac2712692"}}
         :extra-paths ["spec"]
         :main-opts ["-m" "speclj.cli" "-c"]}}}
```

I can then run my tests with `clojure -A:test`. I won't explain all the details
of the alias here, but if you're curious, feel free to look it up in the
documentation I mentioned above.


#### Web servers in Clojure

Web browsers and web servers exchange information using HTTP, [the Hyper-Text
Transfer Protocol](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol).
It's become a pretty dominant protocol, and just about every programming
language has multiple options to choose from when it comes to an HTTP library.

Unfortunately, at least for libraries that help with the server side (responding
to HTTP requests) rather than the client side (initiating requests), the
approaches tend to be pretty all-encompassing, and it can take a long time to
understand what all is happening. Examples of this kind of comprehensive
"framework" approach include Rails (for Ruby) and Django (for Python). (Both of
these languages have less comprehensive options, by the way.) In my opinion,
although it's certainly possible to get work done quickly if you know it,
frameworks like this tend to impede mastery by making the underlying
abstractions relatively difficult to see and appreciate.

In Clojure, the most popular library for serving HTTP responses is
[ring](https://github.com/ring-clojure/ring/wiki). It is, in my opinion, a model
of simplicity. At its
[core](https://github.com/ring-clojure/ring/wiki/Concepts), all it does is
convert an incoming HTTP request to a Clojure hashmap, pass it to the handler
function that you provide, and convert the function's return value from a
Clojure hashmap into an HTTP response to send back to the client. This approach
enables what (again, in my opinion) makes Clojure so fun: pure functions
operating on immutable data structures, because your handler is just a function,
and all the HTTP interactions are managed via Clojure data.

All you have to do is define a
[handler](https://github.com/ring-clojure/ring/wiki/Concepts#handlers), a
function that takes a
[request](https://github.com/ring-clojure/ring/wiki/Concepts#requests) and
returns a
[response](https://github.com/ring-clojure/ring/wiki/Concepts#responses). Then
pass that function to an adapter like `ring.adapter.jetty/run-jetty`, and you
have a web server. Here's some code that does this. Note that you'll need to
depend on the `ring` library (see above) for this code to work; otherwise, the
ring namespaces won't be defined.

```clojure
(ns a-namespace
  (:require [ring.adapter.jetty :refer [run-jetty]]))

(defn handler [request]
  {:status 200  ; meaning "OK"
   :headers {"Content-Type" "text/plain"}  ; instead of e.g. "text/html"
   :body "Hello, world.\n"})  ; the payload

(defn start-server []
  (run-jetty handler {:port 3000}))

(start-server)
```

Try pasting that code into a REPL that you started in a directory with a
`deps.edn` file that depends on `ring`, and you'll have a working web server. In
a separate terminal window, assuming you have [curl](https://curl.haxx.se/)
installed, type `curl http://localhost:3000` to send the request to your little
server. Add the `-v` flag to see more details about the HTTP request and
response. Here's what I see:

```
$ curl -v http://localhost:3000
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 3000 (#0)
> GET / HTTP/1.1
> Host: localhost:3000
> User-Agent: curl/7.64.1
> Accept: */*
>
< HTTP/1.1 200 OK
< Date: Fri, 27 Mar 2020 00:54:48 GMT
< Content-Length: 13
< Server: Jetty(9.4.22.v20191022)
<
* Connection #0 to host localhost left intact
Hello, world.
* Closing connection 0
```

The HTTP request information (generated by `curl`) is indicated with a leading
`> `, and the HTTP response information (generated by `ring` and your handler)
is indicated with a leading `< `. `* ` indicates status messages, and the body
of the response is printed toward the end.

To stop your server (and the REPL), hit control-C. 

Pro tip: [httpie](https://github.com/jakubroztocil/httpie) is an alternative to
curl with some nice features.

Note: please don't add the `:join false` option to the `run-jetty` options, or
the autograder might hang when grading your program.


#### HTML5

Here is a minimal, valid HTML5 document. Line breaks and indentation aren't
necessary; I added them just for readability.

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Test</title>
  </head>
  <body>
    <!-- This is a comment. -->
  </body>
</html>
```

Presumably, some information would go between `<body>` and `</body>`, where the
comment currently is. I'm guessing most of you have a decent understanding of
HTML already. If not, [start
here](https://developer.mozilla.org/en-US/docs/Web/HTML). It shouldn't take long
to understand the basic syntax, and that's all you really need to know for this
assignment. In fact, you might not really need to know even that...

#### Hiccup

(This section is optional. You don't need Hiccup for this assignment. It might
make your life easier, though. And it will definitely come in handy for the next
assignment.)

> Hiccup turns Clojure data structures like this:
> 
> ```clojure
> [:a {:href "http://github.com"} "GitHub"]
> ```
> 
> Into strings of HTML like this:
> 
> ```html
> <a href="http://github.com">GitHub</a>
> ```

(From the [Hiccup Syntax](https://github.com/weavejester/hiccup/wiki/Syntax)
page.)

If you want to know more, start with [the
README](https://github.com/weavejester/hiccup). There's not much to know; it's
pretty simple.

Also note that there's [a function to generate an HTML5
page](http://weavejester.github.io/hiccup/hiccup.page.html#var-html5), like what
we saw above.

Here's a quick example, in a REPL with a deps.edn that includes `hiccup
{:mvn/version "1.0.5"}` in its `:deps` hashmap. (The `user> ` is the REPL
prompt.)

```clojure
user> (require '[hiccup.page :refer [html5]])
nil
user> (html5 [:head [:title "Test"]] [:body [:p {:style "color: green"} "Hello, world."]])
"<!DOCTYPE html>\n<html><head><title>Test</title></head><body><p style=\"color: green\">Hello, world.</p></body></html>"
user> (println *1)  ; to make it a bit easier on the eyes
<!DOCTYPE html>
<html><head><title>Test</title></head><body><p style="color: green">Hello, world.</p></body></html>
nil
```


### Requirements

Unless otherwise noted, all requirements from Assignment 4 still apply.

There are 5 basic requirements for Assignment 5:

1. accept abbreviated addresses
2. implement the `rev-list` command
3. implement the `log --oneline` command
4. implement the `explore` command
5. extend the help output

Details of each requirement follow.


#### 1. accept abbreviated addresses

The following commands can accept an address as an argument:

- `cat-file`, with both the `-t` switch and the `-p` switch
- `commit-tree`, both for the tree address and with the `-p` switches
- `commit`, both for the tree address and with the `-p` switches

In each of these cases, accept addresses with as few as 4 characters. If fewer
than 4 characters are specified, print `"Error: too few characters specified for
address '<addr>'\n"`, where `<addr>` is the given address that's too short.

If, perchance, the given abbreviation is ambiguous (i.e. there is more than one
match for that prefix), print `"Error: ambiguous match for address '<addr>'\n"`,
where `<addr>` is the given address that's ambiguous.

For `commit` and `commit-tree`, first check the tree address, then check the
parent addresses (if any) in the order given.


#### 2. implement the `rev-list` command

Create a new `rev-list` command. If given the `-h` or `--help` switch, print the
following usage screen:

```
idiot rev-list: list preceding revisions, latest first

Usage: idiot rev-list [-n <count>] [<ref>]

Arguments:
   -n <count>   stop after <count> revisions (default: don't stop)
   <ref>        a reference; see the rev-parse command (default: HEAD)
```

Otherwise, if the database directory could not be found, print
<code>"Error: could not find database. (Did you run \`idiot init\`?)\\n"</code>.

Otherwise, if `-n` is given but nothing else, print `"Error: you must specify a
numeric count with '-n'.\n"`.

Otherwise, if `-n` is given but the value is not a non-negative integer, print
`"Error: the argument for '-n' must be a non-negative integer.\n"`

Otherwise, if the `<ref>` argument is an unknown ref, print `"Error: could not
find ref named <ref>.\n"`. Note that `HEAD` and `@` are supported as in
`rev-parse`.

Otherwise, print the chain of full 40-character commit addresses, starting with
the commit pointed to by the given `<ref>` (default: `HEAD`), one per line, as
shown in [git rev-list](#git-rev-list) above. If the `-n` switch is given, stop
after `<count>` commits. Whether `-n` is specified or not, stop after printing a
commit with no parents. If any commit has more than one parent, always follow
the first parent.


#### 3. implement the `log --oneline` command

Create a new `log --oneline` command. If given the `-h` or `--help` switch,
print the following usage screen:

```
idiot log: print abbreviated commit addresses and commit summaries

Usage: idiot log --oneline [-n <count>] [<ref>]

Arguments:
   -n <count>   stop after <count> revisions (default: don't stop)
   <ref>        a reference; see the rev-parse command (default: HEAD)
```

Otherwise, if the `--oneline` is missing or is not the first argument, print
`"Error: log requires the --oneline switch\n"`.

Otherwise, perform the same checks and operations as those of `rev-list` above.
If the the arguments are valid, rather than print the full address of each
commit address, print the first 7 characters of the address, a space, the first
line of the commit message (i.e. everything after the empty line and up to but
not including the first newline `\n` character), and finally a newline.


#### 4. implement the `explore` command

Create a new `explore` command. If given the `-h` or `--help` switch, print the
following usage screen:

```
idiot explore: start a web server to explore the database

Usage: idiot explore [-p <port>]

Arguments:
   -p <port>   listen on the given port (default: 3000)
```

Otherwise, if the database directory could not be found, print
<code>"Error: could not find database. (Did you run \`idiot init\`?)\\n"</code>.

Otherwise, if `-p` is given but nothing else, print `"Error: you must specify a
numeric port with '-p'.\n"`.

Otherwise, if `-p` is given but the value is not a non-negative integer, print
`"Error: the argument for '-p' must be a non-negative integer.\n"`

Otherwise, print `"Starting server on port <port>.\n"`, where `<port>` is the
port specified by `-p`, or, if `-p` is not specified, port 3000. Then start a
web server (see [Web servers in Clojure](#web-servers-in-clojure) above)
listening on port `<port>`. When the server receives any request, it should
respond with an HTML5 document that includes a list of all branches in the
database, similar to the information printed by the `branch` command. (The
[&lt;ul&gt; tag](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ul)
would work well here.) The particulars of tags, attributes, styles, etc. don't
matter, so long as:

- the response has a status of 200;
- the response has a content type of `text/html`;
- there is an HTML5 doctype header at the beginning of the document in the
  response body (see [HTML5](#html5) above);
- the document is well-formed HTML5; and
- every branch name in the database (i.e. every file in the
  `<db-dir>/refs/heads/` directory) is included in the document.


#### 5. extend the help output

First, the `help` command should be extended to include help information for
the new `rev-list`, `log --oneline`, and `explore` commands, as detailed
above.

Second, the usage for the help command should be:

```
idiot help: print help for a command

Usage: idiot help <command>

Arguments:
   <command>   the command to print help for

Commands:
   branch [-d <branch>]
   cat-file {-p|-t} <address>
   commit <tree> -m "message" [(-p parent)...]
   commit-tree <tree> -m "message" [(-p parent)...]
   explore [-p <port>]
   hash-object [-w] <file>
   help
   init
   log --oneline [-n <count>] [<ref>]
   rev-list [-n <count>] [<ref>]
   rev-parse <ref>
   switch [-c] <branch>
   write-wtree
```

Third, the top-level usage info should be printed under the same conditions as
before and should be as follows:

```
idiot: the other stupid content tracker

Usage: idiot [<top-args>] <command> [<args>]

Top-level arguments:
   -r <dir>   run from the given directory instead of the current one
   -d <dir>   store the database in <dir> (default: .idiot)

Commands:
   branch [-d <branch>]
   cat-file {-p|-t} <address>
   commit <tree> -m "message" [(-p parent)...]
   commit-tree <tree> -m "message" [(-p parent)...]
   explore [-p <port>]
   hash-object [-w] <file>
   help
   init
   log --oneline [-n <count>] [<ref>]
   rev-list [-n <count>] [<ref>]
   rev-parse <ref>
   switch [-c] <branch>
   write-wtree
```


### Submission instructions

You will need to submit your code to Gradescope as a Clojure deps project. What
this means is that the top-level directory of the repository or .zip file that
you submit to Gradescope should have a `src` directory with at least an
`idiot.clj` file in it as well as a `deps.edn` file whose contents should be at
least `{:deps {ring/ring {:mvn/version "1.8.0"}}}`. If your submission is
missing these elements, or if they're not in the top-level (root) directory,
then the autograder will not work.

As of Friday, April 10<sup>th</sup>, the autograder **is** available.


### Hints

- The [stuff we've covered in class](../calendar/) should serve you well,
  especially the material on testing, purity/effects, refactoring, and
  debugging.

#### Explore tests

If your submission is timing out, it might be because of some deadlock in the
tests for the explore command. I tried to iron out all such bugs, but it's
possible I missed one. If you suspect this is the case, or if you just want to
run my explore tests locally:

1. put [jst_explore_test.clj](../jst_explore_test.clj) in your `src` directory;
2. put [setup-explore-test.sh](../setup-explore-test.sh) in your project directory (i.e. the one containing `src`);
3. add the following dependencies to the `:deps` key of your your `deps.edn` file:
   ```clojure
   clj-http {:mvn/version "3.10.0"}
   org.clojure/core.async {:mvn/version "1.1.587"}
   org.clojure/data.json {:mvn/version "1.0.0"}
   ```
4. run `clojure -m jst-explore-test` in a bash-compatible shell.

Note that the temporary directory `spec-run-tmp` is preserved when the program
ends so that you can examine things and run your command manually.  (It's also
removed at the beginning of the test to start afresh. If you think you've found
a bug, let me know!
