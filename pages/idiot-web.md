---
title: Assignment 6: Idiot Web
stem: idiot-web
---

### Due

Assignment 6 is due the last second of Friday, April 24<sup>th</sup>. Late
submissions reach their final penalty of 10% after the last second of
Friday, May 8<sup>th</sup>, but are accepted even after that (see the
[syllabus](../syllabus/#assignments) for late policy).

Note that, if you don't take an incomplete, the assignment is absolutely due at
the start of the final exam period, i.e. Tuesday, May 5<sup>th</sup> at 12pm.
Also note that you must let me know if you want an incomplete or CV incomplete
grade. (If that's not what you heard and I should be able to find this out
somehow, let me know and I'll change this note.)


### Bonus opportunity

If your latest submission is at or before the last second of Wednesday, April
22<sup>nd</sup>, you will receive a 5% bonus to the autograded grade.


### Context

Let's finish out the semester by finishing the `explore` command that we
started in A5 to let you navigate around the information in the repository
database, then pointing your explore command at a real git repo.


### Requirements

All requirements from Assignment 5 still apply.

There are two basic requirements for Assignment 6. First, add other endpoints
to your explore command. Second, include a screenshot of your completed Idiot
command exploring your favorite repository not authored by you.

Recall from lecture on Monday, 4/13, that an endpoint is a combination of HTTP
request method and HTTP URI. In this assignment, the supported endpoints will
all have a request method of GET, indicating that they are only reading
information from the server rather than adding information to it.

The supported endpoints are:

1. `/` - `HEAD` info and branch list
2. `/branch/<branch>` - `log --oneline` output with linked commit IDs
3. `/commit/<address>` - show commit, with linked parents and linked tree
4. `/tree/<address>` - show tree, with linked trees and linked blobs
5. `/blob/<address>` - show blob data

In the event that these endpoints return an HTTP response with status 200
("OK"), they should also set the `Content-Type` header to `text/html`, in
preparation for including a response body whose content is a valid HTML5
document.


#### 1. The `/` endpoint: `HEAD` info and branch list

If a GET request is received with a URI of `/`, return an HTTP response with
status 200 ("OK") and body that is an HTML5 document that includes information
about `HEAD` and a branch list.

The `HEAD` information should be in a `div` tag with class `head-info`.

On the assumption that `HEAD` points to a ref, the text contents of the div
should be `"HEAD points to ref <ref>"`, where `<ref>` is the name of the ref
that HEAD points to (e.g. `master`), and the ref name should be linked to
`/branch/<ref>` with an `<a>` tag. You may ignore the case in which `HEAD`
points to a commit; it will not be tested.

The branch list should be in a `<ul>` tag (or "unordered list", which contains
`<li>` tags, or "list items"). The class of the `<ul>` tag should be
`branch-list`. Each child `<li>` should contain a link (with the `<a>` tag)
whose text is the branch name (e.g. `master`) and whose `href` attribute is
`/branch/<ref>`. The branches should be sorted alphabetically by name
(ascending).


#### 2. `/branch/<branch>`: `log --oneline` output with linked commit IDs

This section describes what happens if a GET request is received with a URI of
`/branch/<branch>`.

If there is no ref with a name equal to the branch parsed from the URI, return
an HTTP response with a 404 ("Not Found") status and no body. (Hint: use the
`.startsWith` method of a `java.lang.String` object or clojure.core's
`re-matches` function to test the URI against this pattern.)

Otherwise, return an HTTP response with status 200 ("OK") and a body that is an
HTML5 document that includes effectively the same information as `log
--oneline` reports. In other words, resolve the ref into the commit address it
points to and walk the commit chain as `log --oneline` or `rev-list` does.

Add a `<ul>` tag with a class of `commit-list`. Within this `<ul>`, for each
commit, write an `<li>` tag of the following format: `"<address>
<commit-message-summary>"`, where `<address>` is the abbreviated, 7-character
commit address, which should be linked to `/commit/<address>`, and
`<commit-message-summary>` is the first line of of the commit message. Note
that, except for the link, this is the same information printed by your `log
--oneline` command from A5.


#### 3. `/commit/<address>`: show commit, with linked parents and linked tree

This section describes what happens if a GET request is received with a URI of
`/commit/<address>`. Note that `<address>` will typically be 7 characters long,
as created in the links from endpoints 1 and 2 above, but it can be as few as 4
characters or as many as 40, just as the `idiot` command line tool accepts (see
abbreviated addresses in A5).

If there is no object at that address, return an HTTP response with a 404 ("Not
Found") status and no body.

If there are multiple objects sharing the given `<address>` prefix, return an
HTTP response with a 300 ("Multiple Choices") status and a body that is an
HTML5 document in which there should be a `<p>` tag whose text is `"The given
address prefix is ambiguous. Please disambiguate your intent by choosing from
the following options."`. Following that should be a `<ul>` tag with class
`disambiguation-list` whose `<li>` children are each of the form
`"<full-address> (<type>)"`, where `<type>` is the type of the object (either
`blob`, `commit`, or `tree`) and `<full-address>` is a link whose text is the
full, 40-character address and whose `href` attribute is
`/<type>/<full-address>`.

If there is a single object at that address but it is a tree, return an HTTP
response with a 302 ("Found") status and a `Location` header of
`/tree/<address>`.

If there is a single object at that address but it is a blob, return an HTTP
response with a 302 ("Found") status and a `Location` header of
`/blob/<address>`.

If there is a single object at that address and it is a commit, return an HTTP
response with a 200 ("OK") status and a body that is an HTML document in which
there should be the following information in the following order (which is,
apart from links and HTML tags, the same as that printed by your `cat-file -p`
command):

- A `<h1>` tag whose text is `"Commit <address>"`.
- A `<div>` tag with class `tree` and contents in the form `"tree
  <full-address>"`, where `<full-address>` is the full, 40-character tree
  address. Link the address to `/tree/<full-address>`.
- For each parent (0 or more), a `<div>` tag with class `parent` and contents
  in the form `"parent <full-address>"`, where `<full-address>` is the full
  address of the parent commit. Link the address to `/commit/<full-address>`.
- A `<div>` tag with class `author` of the form `"author <name> &lt;<email>&gt;
  <timestamp>"`, just as the same line is printed by your `cat-file -p` command.
  (Note the characters around the email address. These are the HTML
  entities for the `<` and `>` characters, to indicate that you want the literal
  characters rather than to delimit an HTML tag.)
- A `<div>` tag with class `committer` of the form `"committer <name>
  &lt;<email>&gt; <timestamp>"`, just as the same line is printed by your
  `cat-file -p` command.
- A `<pre>` tag with class `message` with the full commit message.


#### 4. `/tree/<address>`: show tree, with linked trees and linked blobs

This section describes what happens if a GET request is received with a URI of
`/tree/<address>`. As with commits in the previous section, note that
`<address>` may be abbreviated.

If there is no object at that address, return an HTTP response with a 404 ("Not
Found") status and no body.

If there are multiple objects sharing the given `<address>` prefix, do exactly
the same thing as `/commit/<address>` does in that case.

If there is a single object at that address but it is a commit, return an HTTP
response with a 302 ("Found") status and a `Location` header of
`/commit/<address>`.

If there is a single object at that address but it is a blob, return an HTTP
response with a 302 ("Found") status and a `Location` header of
`/blob/<address>`.

If there is a single object at that address and it is a tree, return an HTTP
response with a 200 ("OK") status and a body that is an HTML document in which
there should be the following information in the following order (which is,
apart from links and HTML tags, the same as that printed by your `cat-file -p`
command):

- A `<h1>` tag whose text is `"Tree <address>"`.
- A `<ul>` tag with class `tree-entries` with the tree entries, which should be
  sorted by name as they are in the `cat-file -p` output. For each tree entry,
  add a child `<li>` tag with the following elements separated by one or more
  whitespace characters, and all of which should be wrapped in a `<tt>` tag to
  use a monospaced font:
    - the file mode (e.g. `100644`);
    - the object type (e.g. `blob`);
    - the full, 40-character address, which should be linked to
      `/<type>/<address>`; and
    - the entry name.
    - (For example, the HTML of each `<li>` tag will look something like this: `<li><tt>100644 blob <a href="/blob/1234567890123456789012345678901234567890">1234567890123456789012345678901234567890</a> a-file</tt></li>`.)


#### 5. `/blob/<address>`: show blob data

This section describes what happens if a GET request is received with a URI of
`/blob/<address>`. As with commits in the previous section, note that
`<address>` may be abbreviated.

If there is no object at that address, return an HTTP response with a 404 ("Not
Found") status and no body.

If there are multiple objects sharing the given `<address>` prefix, do exactly
the same thing as `/commit/<address>` and `/tree/<address>` do in that case.

If there is a single object at that address but it is a commit, return an HTTP
response with a 302 ("Found") status and a `Location` header of
`/commit/<address>`.

If there is a single object at that address but it is a tree, return an HTTP
response with a 302 ("Found") status and a `Location` header of
`/tree/<address>`.

If there is a single object at that address and it is a blob, return an HTTP
response with a 200 ("OK") status and a body that is an HTML document in which
there should be the following information in the following order (which is,
apart from links and HTML tags, the same as that printed by your `cat-file -p`
command):

- A `<h1>` tag whose text is `"Blob <address>"`.
- A `<pre>` tag with the blob content.


#### 6. A screenshot of idiot exploring another repo

Include in your submission to Gradescope a screenshot of your `idiot explore`
command operating on your favorite repository not authored by you. You can
point your browser at a commit list or a notable commit. The screenshot should
be named `screenshot.png` and be a PNG file. Also include a `screenshot.txt`
file that briefly describes what the repository is and where one can find it on
the web. I'd enjoy hearing why it's significant to you, if you feel comfortable
sharing.

To do this, you will first need to clone the repository locally. Then you'll
need to use idiot's top-level `-r` switch to tell idiot where to find the
repository directory, as well as a `-d .git` top-level switch to tell Idiot
where to find the database directory within that directory. Finally, your
subprogram should be `explore -p <port>` (see A5), at which point you should go
to `http://localhost:<port>` in your preferred browser.

I've discovered three complications in doing this. First, when I gave an
absolute directory to my top-level `-r` switch, I got an error like this:

> Execution error (IllegalArgumentException) at util.read-tree/eval735$fn
> (read\_tree.clj:37).
> /Users/jeff/code/vendor/clojure is not a relative path

(Of course the Clojure repo is the one that I would pick, right?) The fix was
to copy or move the directory to live under my idiot project directory, e.g. in
the `repo/` subdirectory, then trying again with `-r repo`.

Second, our idiot programs don't support
[packfiles](https://git-scm.com/book/en/v2/Git-Internals-Packfiles), which are
a more efficient way that git uses to store objects. So we'll have to unpack
the objects from the pack file manually. You can see the packfiles in
`.git/objects/packs/`. With a fresh clone, you should only have a single
packfile, which simplifies what you need to do. (If you have more than one
packfile, either clone afresh or repeat the following instructions for each
packfile.) Here's what do to (in a bash-compatible shell):

1. Find the packfile: `ls .git/objects/packs/*.pack`
2. Move it out of the database directory: `mv .git/objects/packs/*.pack .`
3. Unpack the objects: `git unpack-objects < *.pack` (the `<` provides the data
   in the packfile as input to the process rather than an argument, which is
   what the `git unpack-objects` command requires)
4. (Optional) Remove the packfile: `rm -f *.pack`.

Third, I had some complications having to do with assuming blob contents were
strings. It turns out that an accented character in some source code triggered
the old error from A3 that looks like this:

> Execution error (IllegalArgumentException) at util.byte-array/to-str
> (byte\_array.clj:59).
> Value out of range for char: -61

The problem was in my `util.byte-array/to-str` method, which looked like this:

```clojure
(defn to-str
  "Convert a sequence of bytes known to only have printable characters into a
  string. Warning: this is likely to fail if given arbitrary binary data."
  [bytes]
  (->> bytes (map char) (apply str)))
```

Changing it to the following fixed my issue. Your mileage may vary; you may not
even hit this issue depending on which repository you pick.

```clojure
(defn to-str
  "Convert a sequence of bytes known to only have printable characters into a
  string. Warning: this is likely to fail if given arbitrary binary data."
  [bytes]
  (cond
    (= clojure.lang.LazySeq (type bytes)) (->> bytes (map char) (apply str))
    (string? bytes) bytes
    :else (new String #^bytes (byte-array bytes))))
```

Although I am hoping to encourage your good vibes from seeing the fruit of your
semester's labors actually working correctly with a real, live, git repo, I also
don't want you to have to wrestle with complications at the last minute.
Therefore, I will make the screenshot and explanation worth only 1 point. If you
are struggling to get it working on some arbitrary repo, feel free to give up
and ignore that one point.


### Submission instructions

You will need to submit your code to Gradescope as a Clojure deps project. What
this means is that the top-level directory of the repository or .zip file that
you submit to Gradescope should have a `src` directory with at least an
`idiot.clj` file in it as well as a `deps.edn` file whose contents should be at
least `{:deps {ring/ring {:mvn/version "1.8.0"}}}`. If your submission is
missing these elements, or if they're not in the top-level (root) directory,
then the autograder will not work.

As of Saturday, April 18<sup>th</sup> at midnight, the autograder is available.


### Hints

- If you haven't figured out Hiccup yet, you might want to. It will make
  generating HTML much easier.
- You're welcome to use any libraries you want to on this assignment. In
  particular, routing libraries can make it easier to match URIs to handlers
  and pull parameters like `<address>` out of URIs. A popular choice is
  [Compojure](https://github.com/weavejester/compojure), and the new hotness in
  the Clojure community is [Reitit](https://github.com/metosin/reitit). I've
  used both successfully on projects.
- You are welcome to define styles in a CSS file or within the `<style>` tag of
  the `<head>` section of the document to make your web pages look nicer. In
  fact, collaboration on styles is not only allowed but encouraged. If anybody
  came up with some nice styles and shared them on Piazza, that would probably
  be appreciated by more than me. (I'll do it if I have time, but that isn't
  looking likely at this point.)

#### Explore tests

If your submission is timing out, it might be because of some deadlock in the
tests for the explore command. I tried to iron out all such bugs, but it's
possible I missed one. If you suspect this is the case, or if you just want to
run my explore tests locally:

1. put [jst_explore_test_a6.clj](../jst_explore_test_a6.clj) in your `src` directory;
2. put [setup-explore-test-a6.sh](../setup-explore-test-a6.sh) in your project directory (i.e. the one containing `src`);
3. add the following dependencies to the `:deps` key of your your `deps.edn` file:
   ```clojure
   clj-http {:mvn/version "3.10.0"}
   org.clojure/core.async {:mvn/version "1.1.587"}
   org.clojure/data.json {:mvn/version "1.0.0"}
   enlive {:mvn/version "1.1.6"}
   ```
4. run `clojure -m jst-explore-test-a6` in a bash-compatible shell.

Note that the temporary directory `spec-run-tmp` is preserved when the program
ends so that you can examine things and run your command manually.  (It's also
removed at the beginning of the test to start afresh. If you think you've found
a bug, let me know!
