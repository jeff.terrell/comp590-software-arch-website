---
title: Assignment 4: Idiot Switch
stem: idiot-switch
---

### Due

Assignment 4 is due the last second of
~~Monday, March 23<sup>rd</sup>~~
Monday, March 30<sup>th</sup>.
Late submissions reach their final penalty of 10% after the last second of
~~Monday, April 6<sup>th</sup>~~
Monday, April 13<sup>th</sup>, but are accepted even after that
(see the [syllabus](../syllabus/#assignments) for late policy).


### Bonus opportunity

If your latest submission is at or before the last second of Friday, March
27<sup>th</sup>, you will receive a 5% bonus to the autograded grade.


### Context

In [Assignment 3](../idiot-commit/) we created a git clone that can store and
retrieve blob, tree, and commit objects. Now let's implement branching. It's
probably easier than you'd expect.

In fact, this will probably be a fairly light assignment, at least compared to
the last one. You will be prudent to spend time refactoring and reworking your
program to make it more organized. Future assignments will build on this one,
and you don't want to have to start with a mess. Remember what you learned
about writing tests, refactoring, and dealing with effects, including the
"functional core, imperative shell" architecture.


### Storage details

Git refers to branches as _refs_. A ref is just a named reference to a commit.
Refs live in the `.git/refs/heads` directory as text files with the same
filename as ref name. The contents of the text file is the 40-byte text-based
version of the commit address terminated by a newline, so 41 bytes total. For
example, the `master` ref lives in `.git/refs/heads/master` and contains
something like `"9c0376fdc5074a009b403edf467256accf3b6c03\n"`.

The _HEAD_ ref lives at `.git/HEAD`. It's a little different from a regular ref
because it can refer to a commit or another ref. If it refers to a commit, its
contents are the same as a ref's: the 40-byte commit address plus a newline
character. If it refers to a ref, its contents are `ref:`, a space, and the
path to the ref file within the .git database and terminated by a newline. For
example, if `HEAD` refers to the `master` branch/ref, its contents will be
`"ref: refs/heads/master\n"`.

That's it. No binary data. No recursive structures. It's amazingly simple, yet
provides enough capability to make all of `git` possible.

You'll be mimicking `git` as usual, but of course you won't be using `.git` as
the database directory unless the user specifically calls the program that way.


### Requirements

Unless otherwise noted, all requirements from Assignment 3 still apply.

There are 6 basic requirements for Assignment 4:

1. extend the `init` command to include ref data
2. implement the `rev-parse` command
3. implement the `switch` command
4. implement the `branch` command
5. implement the `commit` command
6. extend the help output

Details of each requirement follow.


#### 1. extend the `init` command

_(The following assumes a default value for the `-d` switch. If a non-default
`-d` value is given, make the following changes in that directory instead of
`.idiot`.)_

In addition to creating the `.idiot/objects` directory, also:

- create the `.idiot/refs` directory and, within that, the empty
  `.idiot/refs/heads` directory.
- create the `.idiot/HEAD` file with initial content `"ref:
  refs/heads/master\n"`.

Note that, when you first `init`ialize a new idiot repository, `HEAD` will
point to `master` even though `master` does not exist. In that case, all git
commands, including `branch`, `switch`, and `rev-parse` below, should consider
`master` _not_ to exist, even though `HEAD` points to it.


#### 2. implement the `rev-parse` command

Create a new `rev-parse` command. If given the `-h` or `--help` switch, print
the following usage screen:

```
idiot rev-parse: determine which commit a ref points to

Usage: idiot rev-parse <ref>

<ref> can be:
- a branch name, like 'master'
- literally 'HEAD'
- literally '@', an alias for 'HEAD'
```

Otherwise, if no arguments are given, print `"Error: you must specify a branch
name.\n"`.

Otherwise, if more than one argument is given, print `"Error: you must specify
a branch name and nothing else.\n"`.

Otherwise, if the database directory could not be found, print
<code>"Error: could not find database. (Did you run \`idiot init\`?)\\n"</code>.

Otherwise, if the argument is `HEAD` or `@`, and the repo HEAD points to a
commit, print the commit address followed by a newline.

Otherwise, if the argument is `HEAD` or `@`, and the repo HEAD points to a ref,
print the commit address that the branch points to followed by a newline.

Otherwise, if the argument is an unknown ref, print `"Error: could not find ref
named <name>.\n"`, where `<name>` is the given argument.

Otherwise, print the 40-character commit address pointed to by the named ref,
followed by a newline. For example, `idiot rev-parse master` should print the
commit address pointed to by the `master` ref, followed by a newline.


#### 3. implement the `switch` command

Create a new `switch` command. If given the `-h` or `--help` switch, print the
following usage screen:

```
idiot switch: change what HEAD points to

Usage: idiot switch [-c] <branch>

Arguments:
   -c   create the branch before switching to it
```

Otherwise, if no arguments are given, print `"Error: you must specify a branch
name.\n"`.

Otherwise, if more than one argument besides the optional `-c` switch is given,
print `"Error: you may only specify one branch name.\n"`.

Otherwise, if the database directory could not be found, print
<code>"Error: could not find database. (Did you run \`idiot init\`?)\\n"</code>.

Otherwise, if the `-c` switch is present and a ref with the given name already
exists, print `"Error: a ref with that name already exists.\n"`.

Otherwise, if the `-c` switch is not present and no ref with the given name
exists, print `"Error: no ref with that name exists.\n"`.

Otherwise, if the `-c` switch is not given, update HEAD to point to the given
ref and print `"Switched to branch '<branch>'\n"`, where `<branch>` is the
given ref name.

Otherwise, if the `-c` switch is given, create a new ref with the given name
that points to the commit that HEAD currently points to, then update HEAD to
point to the newly created ref and print `"Switched to a new branch
'<branch>'\n"`, where `<branch>` is the given ref name. For example, if HEAD
points to the `master` ref, and the `master` ref points to commit C, and the
command `idiot switch -c my-branch` is run, then (1) a new ref `my-branch`
should be created pointing to commit C, (2) the `master` ref should continue to
point to commit C, and (3) HEAD should point to ref `my-branch`.


#### 4. implement the `branch` command

Create a new `branch` command. If given the `-h` or `--help` switch, print the
following usage screen:

```
idiot branch: list or delete branches

Usage: idiot branch [-d <branch>]

Arguments:
   -d <branch>   delete branch <branch>
```

Otherwise, if `-d` is given but nothing else, print `"Error: you must specify a
branch name.\n"`.

Otherwise, if arguments are given and they are invalid (i.e. if the arguments
are anything besides `-d` and exactly one other argument), print `"Error:
invalid arguments.\n"`.

Otherwise, if the database directory could not be found, print
<code>"Error: could not find database. (Did you run \`idiot init\`?)\\n"</code>.

Otherwise, if the `-d` switch is present and no ref with the given name exists,
print `"Error: branch '<branch>' not found.\n"`, where `<branch>` is the given
branch name.

Otherwise, if the `-d` switch is given and HEAD is currently pointing to the
same branch, print `"Error: cannot delete checked-out branch '<branch>'.\n"`,
where `<branch>` is the given branch name.

Otherwise, if the `-d` switch is given, delete the given ref and print
`"Deleted branch <branch>.\n"`, where `<branch>` is the given branch name.

Otherwise, print the branches in sorted order, one per line, with a `* ` prefix
indicating the branch that HEAD points to, if any, and a prefix of two spaces
otherwise.  For example, with the following three refs existing and HEAD
pointing to `master`, the following output would be printed:

```
  add-undo-redo
  fix-long-lines
* master
```


#### 5. implement the `commit` command

Create a new `commit` command. If given the `-h` or `--help` switch, print the
following usage screen (whose arguments are identical to those of
`commit-tree`):

```
idiot commit: create a commit and advance the current branch

Usage: idiot commit <tree> -m "message" [(-p parent)...]

Arguments:
   -h               print this message
   <tree>           the address of the tree object to commit
   -m "<message>"   the commit message
   -p <parent>      the address of a parent commit
```

Otherwise, perform the same checks and operations as those of `commit-tree`. If
the commit object was successfully created, rather than print the address of
the new commit, print `"Commit created.\n"`.

If HEAD points to a ref rather than a commit, update the ref pointed to by HEAD
so that it points to the new commit, and print (in addition to the message
above) `"Updated branch <branch>.\n"`, where `<branch>` is the branch pointed
to by HEAD. If the ref pointed to by HEAD does not exist (which happens e.g.
when first initializing the repository), you will need to create it, and it
should still point to the new commit.


#### 6. extend help output

First, the `help` command should be extended to include help information for
the new `rev-parse`, `switch`, `branch`, and `commit` commands, as detailed
above.

Second, the usage for the help command should be:

```
idiot help: print help for a command

Usage: idiot help <command>

Arguments:
   <command>   the command to print help for

Commands:
   branch [-d <branch>]
   cat-file {-p|-t} <address>
   commit <tree> -m "message" [(-p parent)...]
   commit-tree <tree> -m "message" [(-p parent)...]
   hash-object [-w] <file>
   help
   init
   rev-parse <ref>
   switch [-c] <branch>
   write-wtree
```

Third, the top-level usage info should be printed under the same conditions as
before and should be as follows:

```
idiot: the other stupid content tracker

Usage: idiot [<top-args>] <command> [<args>]

Top-level arguments:
   -r <dir>   run from the given directory instead of the current one
   -d <dir>   store the database in <dir> (default: .idiot)

Commands:
   branch [-d <branch>]
   cat-file {-p|-t} <address>
   commit <tree> -m "message" [(-p parent)...]
   commit-tree <tree> -m "message" [(-p parent)...]
   hash-object [-w] <file>
   help
   init
   rev-parse <ref>
   switch [-c] <branch>
   write-wtree
```


### Submission instructions

(Same as before.) You will need to submit your code to Gradescope as a Clojure
deps project. What this means is that the top-level directory of the repository
or .zip file that you submit to Gradescope should have a `src` directory with
at least an `idiot.clj` file in it as well as a `deps.edn` file whose contents
should be at least `{}`. If your submission is missing these elements, or if
they're not in the top-level (root) directory, then the autograder will not
work.

As of Thursday, March 5<sup>th</sup>, the autograder is not available.


### Hints

- How can you reuse the logic of `commit-tree` for the new `commit` command?
