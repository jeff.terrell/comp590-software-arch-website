---
title: Assignments
stem: assignments
---

Assignments are due at the last second of the indicated due date. See [the
syllabus](../syllabus/) for the late policy.

| # | assignment                               | due        |
|---|------------------------------------------|------------|
| 1 | [Clojure practice](../clojure-practice/) | Mon Feb 3  |
| 2 | [Idiot Init](../idiot-init/)             | Mon Feb 17 |
| 3 | [Idiot Commit](../idiot-commit/)         | Mon Mar 23 |
| 4 | [Idiot Switch](../idiot-switch/)         | Mon Mar 30 |
| 5 | [Idiot List](../idiot-list/)             | Tue Apr 14 |
| 6 | [Idiot Web](../idiot-web/)               | Fri Apr 24 |
