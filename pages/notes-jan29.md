---
title: Notes from Jan. 29 lecture
stem: notes-jan-29
---

### Announcements

-   music: _More Than Words_ by Extreme
    -   we'll be talking about words a lot today...
    -   plus I wanted to give us all a taste of the softer side of hair metal
-   only 13 of you have tried submitting assignment 1 so far
    -   I hope you don't have issues because there aren't many office hours
        left between now and the deadline!
-   note: Clojure has a single-pass compiler, so function order matters, and a
    top-level function like `-main` will need to be the last thing defined in
    your namespace because it uses the definitions that come before it
-   your own 1-page cheat sheet is allowed on the exam next Wednesday

### Why abstract away details?

-   because brains have a finite capacity for attention
-   study: we can pay attention to at most 7 things at a time, plus or minus 2
-   remember: code is for humans, not computers
-   motivating example: in a freelance project several years ago, I started
    with a single file with over 3,000 lines of Javascript. The detail is
    overwhelming. By the end, I had broken the code out into several dozen
    modules, and it was much easier to deal with.

### Abstraction in human languages

-   think about what comes to mind when I say the word "elephant"
-   (kind of amazing that we can thought-share via words, no?)
-   you didn't think about all that makes an elephant: bones, tissues,
    behaviors, thoughts, feelings, migration patterns, group dynamics, elephant
    taming, etc.
-   this is on purpose, so that we can get things done without getting bogged
    down in details
-   also, our finite brains can't hold all the details--we _need_ to abstract
-   how about "Chapel Hill"? Same thing. A high-level idea that we can convey
    without getting bogged down about all that makes CH CH.
-   human words point to _meaning_ (in a loose, fuzzy way)
-   definitions are _boundaries_, putting limits on what range of meaning can
    be intended by a word
-   another one: "calculus"
-   we have a sense of the boundary there, but note that the boundary in this
    case is purely in the realm of ideas, not physical reality
-   not all words work this way exactly; the class of words that do can be
    called "names"

### Hierarchy in names and ideas

-   the idea of hierarchy in names is very practical, given our finite brains
-   we can talk about "subjects" and "math" and "calculus" and "series" and
    "convergent series"
-   we're able to talk about things at various levels of abstraction; we ignore
    details as needed to stay at a certain level
-   this is useful: convey a sense of the forest without getting bogged down in
    trees
-   we can call this the "ladder of abstraction", and each word can live on a
    particular rung of the ladder, with more abstract things above it and more
    concrete things below it
-   Paul Graham (I think it was) said that programmers are somewhat unique, as
    a rule, in that they are more able to quickly climb up and down the ladder
    as needed

### Names and hierarchy in code

-   we can name values (e.g. with a `let` binding or var `def`inition in
    Clojure)
-   we can name functions (e.g. with `defn`)
-   we can name groups of functions
-   grouping functions into _modules_ is a key strategy to maintain order when
    programs get bigger
-   assignment 1 is probably small enough not to need modules
-   later assignments will benefit more and more from good module boundaries
-   in Clojure, modules are called namespaces: a group of vars and statements
-   in Java, modules are usually called classes
-   in Javascript, modules are called modules
-   typically, 1 module = 1 file of code
-   in Clojure, you don't have to do that--can have a second `(ns)` form
    halfway through a file if you want--this requires a simpler, single-pass
    compiler in Clojure, which is a deliberate design decision, I think because
    it works well with a REPL, which is inherently single-pass
-   but you should probably stick with 1 namespace per file unless you know
    what you're doing

### Quiz: Why is abstraction important in language?

> Because it accommodates our human limitations.

### The art of defining module boundaries

-   how do you decide where your module boundaries are?
-   key assumption: you're able to move complexity around
-   first, group code elements (e.g. functions) together, like with like
-   second, name the groups
-   a good name:
    -   uses words that connect with meaning at the level of human language,
        e.g. "utility", "sequence", "database-connection", etc.
    -   applies to everything in that namespace
    -   is as narrow a boundary as possible
-   third: rearrange the elements, move the boundaries, and adjust the names as
    needed to find fitting names for every module
-   goal: _cohesion_, i.e. everything in a module is closely related
-   cohesion is a property of the module based on the stuff within it, not its
    relationship to other modules, i.e. it's an "intra" rather than "inter"
    property
-   a test: the _single responsibility principle_: can you describe the
    responsibility of this module simply, in a single statement, without using
    the word "and"? If not, maybe you haven't found a good boundary.

### Good modules help remove incidental complexity

-   when like things are with like things, we might notice repeated patterns
-   a repeated pattern means that there is some noise, some incidental
    complexity
-   consolidating the pattern into a single place means that we can move the
    noise elsewhere
-   this not only reduces the total complexity, it also can reveal other
    patterns
-   (an admittedly contrived) example:
    -   initially:

        ```javascript
        var len1 = arr1.length
        var sum1 = 0
        for (var i = 0; i < len1; i++) {
          sum1 += arr1[i]
        }
        var avg1 = sum1 / len1
        var len2 = arr2.length
        var sum2 = 0
        for (var i = 0; i < len2; i++) {
          sum2 += arr2[i]
        }
        var avg2 = sum2 / len2
        var len3 = arr3.length
        var sum3 = 0
        for (var i = 0; i < len3; i++) {
          sum3 += arr3[i]
        }
        var avg3 = sum3 / len3
        ```

    -   we notice that the for loops are all similar; let's remove the summing
        to a new function:

        ```javascript
        var sum = (arr) => {
          var sum = 0
          var len = arr.length
          for (var i = 0; i < len; i++) {
            sum += arr[i]
          }
          return sum
        }
        var len1 = arr1.length
        var sum1 = sum(arr1)
        var avg1 = sum1 / len1
        var len2 = arr2.length
        var sum2 = sum(arr2)
        var avg2 = sum2 / len2
        var len3 = arr3.length
        var sum3 = sum(arr3)
        var avg3 = sum3 / len3
        ```

    -   now we see other patterns; let's pull the pattern out into a new `avg`
        function

        ```javascript
        var sum = (arr) => {
          var sum = 0
          var len = arr.length
          for (var i = 0; i < len; i++) {
            sum += arr[i]
          }
          return sum
        }
        var avg = (arr) => sum(arr) / arr.length
        var avg1 = avg(arr1)
        var avg2 = avg(arr2)
        var avg3 = avg(arr3)
        ```

    -   we still have a pattern: three var assignments and 3 calls to `avg`;
        let's pull that out by calling `map`

        ```javascript
        var sum = (arr) => {
          var sum = 0
          var len = arr.length
          for (var i = 0; i < len; i++) {
            sum += arr[i]
          }
          return sum
        }
        var avg = (arr) => sum(arr) / arr.length
        var avgs = [arr1, arr2, arr3].map(avg)
        ```

    -   finally, let's replace the `for` loop in the body of `avg` with a call
        to `reduce`:

        ```javascript
        // in Clojure, we'd just say + here as 1st arg to reduce...
        var sum = (arr) => arr.reduce((a,b) => a + b, 0)
        var avg = (arr) => sum(arr) / arr.length
        var avgs = [arr1, arr2, arr3].map(avg)
        ```

#### Lessons:

-   patterns are difficult to see when too much noise is present
-   consolidating a certain type of noise into one spot means that other
    instances of that noise can be eliminated
-   higher-order functions (e.g. functions that take functions as arguments)
    are powerful tools to embody a pattern
-   names matter! Imagine naming `sum` `f` instead--it wouldn't be clear from
    the name what it does or when to use it. And `add` would not be a good name
    either, because you might expect to call it like add(1,2) instead of
    add(arr).
-   noticing the patterns was possible because the instances were all next to
    each other. Imagine if they were strewn all over a code base.

### Subjectivity in naming

-   cohesion and the SRP are fuzzy, inexact principles, qualitative rather than
    quantitative
-   this is more an art than a science
-   people have different ideas about what words mean (although broad
    agreement)
-   some words are precise, but specialized and not known by many, e.g.
    "monad"; I say that's OK because the name still means something precise,
    even if people don't already know the meaning
-   there's no right answer, and perfection is an elusive goal
-   yet the pursuit is worthy and makes great strides to reducing incidental
    complexity

### Module "smells"

-   it's a bunch of random stuff, with no unifying theme
    -   then the name doesn't help you think about the high-level abstract idea
        divorced from the details
    -   although, all code has to live somewhere, and having a small
        "miscellaneous" module might be preferable to having ill-fitting
        functions scattered around your codebase
-   the name connects to meaning but doesn't fit all that's in there
    -   this disrupts your connection to meaning as you read the code
    -   have to keep in mind the implicit asterisk whenever you see the module
        name, which takes up space in your finite attention
-   it's too big (in concept or in the number of elements it contains)
    -   advice: split it into sub-groups, but keep the larger name as well
    -   e.g. in Clojure you could have calculus.limits, calculus.series,
        calculus.derivatives, etc. The files would live in the "calculus/"
        directory. And you could still have a top-level calculus namespace if
        needed.
-   it's too small (e.g. just 1-2 functions in it)
    -   this is not always a bad thing
    -   main things to think about:
        -   how quickly can you get a sense of whether a new function you
            create might belong here?
        -   how many top-level module names do you have to read to get a
            high-level view of the entire codebase? 5-10, not bad; 100 could be
            painful.
-   remember: high-level names should help us connect to meaning without
    overwhelming detail, as a concession to our finite attention

### Take-away quiz:

-   What new knowledge are you walking away with today?
-   What concepts were confusing or unclear?
-   Any other feedback for Jeff?
