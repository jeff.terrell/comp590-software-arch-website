---
title: Assignment 3: Idiot Commit
stem: idiot-commit
---

### Due

Assignment 3 is due the last second of
~~Monday, March 2<sup>nd</sup>~~
~~Thursday, March 5<sup>th</sup>~~
Monday, March 23<sup>rd</sup>.
Late submissions accepted until the last second of
~~Monday, March 16<sup>th</sup>~~
~~Thursday, March 19<sup>th</sup>~~
Monday, April 6<sup>th</sup>
(see the [syllabus](../syllabus/#assignments) for late policy).


### Bonus opportunity

If your latest submission is at or before the last second of
~~Friday, February 28<sup>th</sup>~~
~~Monday, March 2<sup>nd</sup>~~
Friday, March 20<sup>th</sup>, you will receive a 5% bonus to
the autograded grade.


### Context

Building on the foundation of [Assignment 2](../idiot-init/), let's add the
ability to store tree objects and commit objects to our object database and
retrieve them from it. Recall that tree objects are snapshots of directory
contents, just like blob objects are snapshots of file contents. Also recall
that commit objects refer to a tree and include metadata about the author,
timestamp, message, and parent commits, if any.

This assignment will be the foundation that later assignments are built on. The
next assignment will assume that this one is finished. So, even after getting
full credit from the autograder, it might be prudent to spend some time
refactoring, while the details are relatively fresh in mind.

Conversely, if you did not refactor or structure the code for Assignment 2
well, you may want to spend some time cleaning things up before adding new
functionality.


### Storage details

We've already seen blob objects. Recall how they are stored in the object
database. A header is computed for the blob that includes the object type
(`blob`), a space (`" "`), an object length encoded as a string (e.g.  `123`),
and a NUL character (`"\000"` in a Clojure string). The header is combined with
the blob itself. The header+blob combination is hashed with the SHA1 algorithm
to compute a 40-character hexadecimal address. Then the header+blob combination
is stored, compressed with the ZLIB library, at
`<database-dir>/objects/<first2>/<last38>`, where `<database-dir>` is the
database directory (e.g. `.git`), `<first2>` is the first 2 characters of the
address and `<last38>` is the last 38 characters.

We will follow this same scheme for tree objects and commit objects. The header
includes the object type, a space, the object length, and a NUL character. The
header is combined with the object itself to compute an address. The
header+object is stored, compressed, at the location in the database directory
corresponding to the computed address.


#### Tree objects

Trees are snapshots of directory contents. Just like directories can contain
files and directories, trees can refer to blobs and trees.

A tree consists of one or more entries. There is one entry for each object in
the tree, either a blob or a tree. Each entry looks like this:

    <mode> <path>\000<address>

In other words, the mode, then a space, then the path, then a NUL character,
then the SHA1 address. `<mode>` has to do with metadata about the file or
directory, including permissions, but for our purposes use `040000` for
directories and `100644` for files. `<path>` is the name of the file or
directory in this tree. (Note: this is the only place where path information is
stored in git.) `<address>` is the location of the content in git, given by the
same SHA1 hashing algorithm as before, but instead of being encoded in a
40-character hexadecimal string, it's encoded in binary and therefore 20 bytes.

As a simple example, say the root repository directory contains only a file
named `file` with contents `file contents` terminated by a newline character.
The file's address is `d03e2425cf1c82616e12cb430c69aaa6cc08ff84`. You can try
this with `git` in a bash-compatible shell:

```
mkdir tmp
cd tmp
git init
echo "file contents" > file  # appends a newline automatically
git add file
git write-tree  # should print cde220501bf8f8bb1e33dc49c3c0860e6f5a1132
git cat-file -p cde220501bf8f8bb1e33dc49c3c0860e6f5a1132
# or, if you have `hexdump` installed, view the raw bytes (without the header):
git cat-file tree cde220501bf8f8bb1e33dc49c3c0860e6f5a1132 | hexdump -C
```

(Note that `git write-tree` is a little different that `idiot write-wtree`
because it operates on the index, which is what gets committed when you say
`git commit`. This is why we say `git add file` above. `idiot` doesn't have an
index, so we just make a tree from whatever is in the working directory.)

(Also note: as you're comparing your files to what git created, don't be
alarmed if the contents of the zipped object files on disk aren't exactly equal
to what your program is doing. I think the only differences are related to the
zipping and are inconsequential. The unzipped contents are what should be
exactly equal.)

Now, let's move the file into a different directory.

```
git rm --cached file  # remove the file from the index
mkdir dir
mv file dir
git add dir/file
git write-tree  # should print 7bfe0b8771286dee5ce7cb50a9e85f6c4ddbd712
git cat-file -p 7bfe0b8771286dee5ce7cb50a9e85f6c4ddbd712
```

Notice that the only entry in the new tree is a reference to the old tree. This
make sense because the contents of the `dir` directory are exactly the same as
the contents of the old root tree.

Let's see some Clojure code to compute the same addresses. You can paste this
code into a REPL with no dependencies to see the same addresses printed.

```clojure
(import 'java.security.MessageDigest)

(defn sha-bytes [bytes]
  (.digest (MessageDigest/getInstance "sha1") bytes))

(defn to-hex-string
  "Convert the given byte array into a hex string, 2 characters per byte."
  [bytes]
  (letfn [(to-hex [byte]
            (format "%02x" (bit-and 0xff byte)))]
    (->> bytes (map to-hex) (apply str))))

(def blob-contents "file contents\n")
(def blob-addr (sha-bytes (.getBytes (str "blob 14\000" blob-contents))))
(to-hex-string blob-addr) ; d03e2425cf1c82616e12cb430c69aaa6cc08ff84
(def tree-contents
  (byte-array (concat (.getBytes "tree 32\000100644 file\000")
                      blob-addr)))
(def tree-addr (sha-bytes tree-contents))
(to-hex-string tree-addr) ; cde220501bf8f8bb1e33dc49c3c0860e6f5a1132
(def tree2-contents
  (byte-array (concat (.getBytes "tree 30\00040000 dir\000")
                      tree-addr)))
(def tree2-addr (sha-bytes tree2-contents))
(to-hex-string tree2-addr) ; 7bfe0b8771286dee5ce7cb50a9e85f6c4ddbd712
```

There's a subtlety in the definition of `tree2-contents`: the leading zero is
stripped from the mode, so that the mode only has the 5 characters `40000`
instead of the 6 characters `040000`. This is how git stores it, and you will
need to store it the same way to pass the tests. However, when you print the
tree with `git cat-file -p`, you should print the leading `0`, which is also
what git does.

Lastly, note that tree entries should be sorted alphabetically by name.


#### Commit objects

A commit object is a lot like a raw email message. There are headers, each of
which contains a name and a value, and after that comes a message. Here's an
example:

```
tree 58dce97c8fc359ae58c6b631c383397f7d313d56
parent 511f3497a344839770faf73455a4ec5775710be3
author Jeff Terrell <terrell@cs.unc.edu> 1581997446 -0500
committer Jeff Terrell <terrell@cs.unc.edu> 1581997446 -0500

Add A1 solutions
```

The `tree` header is required, and its value is the address of a tree object,
which represents the snapshot of repository contents at that commit.  The
`author` and `committer` headers are required, and, like the example, should
have the same values. (These values can vary in real-world uses, but we don't
need to support that case.) The values include the author/committer name, email
address, timestamp (in Unix form, i.e. the number of seconds since 1969), and
time zone. For this assignment, hardcode the author and committer values as
`Linus Torvalds <torvalds@transmeta.com> 1581997446 -0500`. (We may use a real
timestamp later, but for now I want to avoid race conditions in the test
suite.)

The `parent` header is optional. If it isn't present, the commit has no
parents&mdash;a situation which usually implies that it is the initial commit
of a repository. 1 parent header is the typical scenario for most commits. 2 or
more parent headers indicate a merge commit, where two different histories are
brought together. If there is more than 1 parent, include a `parent` line in
the commit headers for each parent; for example, if there are 8 parents (an
"octopus merge"&mdash;see the GitHub logo), there should be 8 `parent` lines in
the commit object.

After the headers comes an empty line followed by the message, which can span
multiple lines as necessary. The message will always end in a newline; in fact,
whatever is passed in from the command line as the message will always
automatically get a newline appended.

You can use the following code to generate a commit object. Resuming from the
example in the previous section:

```
export GIT_AUTHOR_NAME="Linus Torvalds"
export GIT_AUTHOR_EMAIL="torvalds@transmeta.com"
export GIT_AUTHOR_DATE="1581997446 -0500"
export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"
export GIT_COMMITTER_DATE="$GIT_AUTHOR_DATE"
git commit-tree -m "Initial commit" 7bfe0b8771286dee5ce7cb50a9e85f6c4ddbd712
git cat-file -p 997d438bb88d1a8f13e3d075b475214bebfb6103
```

To recreate the same address in Clojure, try the following in the same REPL
session as you used for the previous section.

```clojure
(def commit-object
  (let [author-str "Linus Torvalds <torvalds@transmeta.com> 1581997446 -0500"
        tree-str (to-hex-string tree2-addr)
        message "Initial commit"
        commit-format (str "tree %s\n"
                           "author %s\n"
                           "committer %s\n"
                           "\n"
                           "%s\n")
        commit-str (format commit-format
                           tree-str
                           author-str
                           author-str
                           message)]
    (format "commit %d\000%s"
            (count commit-str)
            commit-str)))

(def commit-addr (sha-bytes (.getBytes commit-object)))
(to-hex-string commit-addr) ; 997d438bb88d1a8f13e3d075b475214bebfb6103
```

Note that the given message should have a newline appended to it when it's part
of the commit.


### Requirements

Unless otherwise noted, all requirements from Assignment 2 still apply.

(Note that the code for existing commands might not pass the test cases for
those commands in the autograder, because the autograder tries to do as many
things as possible in a temporary directory to avoid polluting your source
directory.  So, you may want to tackle the `-r` switch first (see #4 below).)

There are 7 basic requirements for Assignment 3:

1. Add a `write-wtree` command to write tree objects
2. Add a `commit-tree` command to write commit objects
3. Add a `-t` switch to the `cat-file` command to print the object type
4. Add a top-level `-r` switch to change to a different repository directory
   before running the command
5. Add a top-level `-d` switch to control where the object directory lives (and
   change the default object directory from `.git` to `.idiot`)
6. Extend `cat-file -p` to work with trees and commits
7. Extend the help message output

Details of each requirement follow.


#### 1. The `write-tree` command

If `idiot` is invoked with `write-wtree` as the first argument, the write-wtree
command is invoked. If a further argument of `-h` or `--help` is given, print
the following usage message:

```
idiot write-wtree: write the working tree to the database

Usage: idiot write-wtree

Arguments:
   -h       print this message
```

Otherwise, if extra arguments are given, print `"Error: write-wtree accepts no
arguments\n"`.

Otherwise, if the database directory could not be found, print
<code>"Error: could not find database. (Did you run \`idiot init\`?)\\n"</code>.

Otherwise, create a new tree object for the contents of the current directory
(respecting the top-level `-r` switch). The directory at the root of the
repository is called the "working tree" in git, hence the name `write-wtree`.
(Git has a `write-tree` command, but it writes a new tree based on the contents
of the index, and we won't be implementing the index.)

Note that:

1. you will need to write all of the contents of the tree before writing the
   tree itself, since the tree's address depends on the addresses of its
   entries;
2. directories that would result in an empty tree, either because they have no
   contents or because any contained directories would themselves result in an
   empty tree, should be skipped;
3. some blob or tree objects in your tree will already have been written by a
   previous call to `hash-object -w` or `write-wtree`, and in such cases you
   should skip that object and not overwrite what has already been stored; and
4. the entries for a particular tree object should be sorted alphabetically by
   name to ensure a consistent address.

If there was nothing to be saved because the directory is empty (apart from the
`.idiot` directory or whatever is specified by the `-d` option), print `"The
directory was empty, so nothing was saved.\n"`.

Otherwise, when the tree has been written to the object database, print the
address of the newly created tree object (i.e. the one that corresponds to the
repository root directory, not one that corresponds to a subdirectory) followed
by a newline.


#### 2. The `commit-tree` command

If `idiot` is invoked with `commit-tree` as the first argument, the commit-tree
command is invoked. If a further argument of `-h` or `--help` is given, print
the following usage message:

```
idiot commit-tree: write a commit object based on the given tree

Usage: idiot commit-tree <tree> -m "message" [(-p parent)...]

Arguments:
   -h               print this message
   <tree>           the address of the tree object to commit
   -m "<message>"   the commit message
   -p <parent>      the address of a parent commit
```

Otherwise, if the database directory could not be found, print
<code>"Error: could not find database. (Did you run \`idiot init\`?)\\n"</code>.

Otherwise, if no tree address is given, print `"Error: you must specify a tree
address.\n"`.

Otherwise, if no object exists at the given tree address, print `"Error: no
tree object exists at that address.\n"`.

Otherwise, if an object exists at the given tree address but it isn't a tree
object, print `"Error: an object exists at that address, but it isn't a
tree.\n"`.

Otherwise, if no `-m` switch is given, print `"Error: you must specify a
message.\n"`.

Otherwise, if the `-m` switch is given but no further arguments are given,
print `"Error: you must specify a message with the -m switch.\n"`.

Otherwise, if any of the given parent addresses don't correspond to stored
objects, print `"Error: no commit object exists at address <addr>.\n"`, where
`<addr>` is the first parent address that does not have an object.

Otherwise, if any of the given parent addresses refer to an object that isn't a
commit object, print `"Error: an object exists at address <addr>, but it isn't
a commit.\n"`, where `<addr>` is the first parent address that has a non-commit
object.

Otherwise, if the last `-p` switch is given but no further arguments are given,
print `"Error: you must specify a commit object with the -p switch.\n"`.

Otherwise, write a new commit object with the given `tree`, `parent`(s) (if
any), the hardcoded author and committer lines, and the given
message&mdash;all in that order&mdash;and print the address of the new commit
object followed by a newline. See the [Commit objects](#commit-objects) section
for more details. Note that parents must be stored in the order given,
otherwise your address will not be correct.


#### 3. The `cat-file -t` switch

The updated `cat-file` usage message, given when the `cat-file` command is
invoked with `-h` or `--help`, is:

```
idiot cat-file: print information about an object

Usage: idiot cat-file {-p|-t} <address>

Arguments:
   -h          print this message
   -p          pretty-print contents based on object type
   -t          print the type of the given object
   <address>   the SHA1-based address of the object
```

As with the existing `-p` switch, if `-t` is given without an address, print
`"Error: you must specify an address.\n"`.

Now, instead of the `-p` switch being required, either the `-p` switch or the
`-t` switch is required. The corresponding error message should be `"Error:
either the -p or the -t switch is required.\n"`.

The command should still check that the given address corresponds to a object
in the database, and if not, it should print `"Error: that address doesn't
exist.\n"`.

If the `-t` switch is given, print the object type followed by a newline. The
object type is one of `blob`, `tree`, or `commit`.


#### 4. The top-level `-r` switch

Before the command is given to `idiot`, a `-r` switch may be specified along
with a directory, in which case the program is run from the context of another
directory. This means that everything done by the program is done from the
context of that directory, including:

- finding the database directory (cf. the next section)
- finding the file given to `hash-object -w`
- deciding which trees and blobs to write in the `write-wtree` command

For example, `idiot -r test hash-object -w a-file` will attempt to store the
contents of the `test/a-file` file as a blob in the ~~`test/.data`~~
`test/.idiot` object directory.

If the `-r` switch is given but no further argument exists, print `"Error: the
-r switch needs an argument\n"`.

If the `-r` switch refers to a directory that does not exist, print `"Error:
the directory specified by -r does not exist\n"` and do not run the given
command.

If unspecified, all operations occur relative to the current directory, as
before.


#### 5. The top-level `-d` switch

Before the command is given to `idiot`, either before or after the `-r` switch
(see previous section), a `-d` switch may be specified along with a directory, in which case the program looks for the database in that directory.

If the `-d` and `-r` switches are both given, the program takes both values
into account when looking for the database. For example, `idiot -r tmp-dir -d
.my-idiot init` will create a new database in the directory
`tmp-dir/.my-idiot`.

If the `-d` switch is given but no further argument exists, print `"Error: the
-d switch needs an argument\n"`.

If unspecified, the program will look for the database in the `.idiot`
directory. Note that this is a change in the behavior from Assignment 1, where
the default was `.git`. This way, running idiot is unlikely to interfere with
or use your actual git database (unless you specifically tell it to), so that
you can use git to manage your assignment source code and still test your
fledgling idiot program.


#### 6. Extend `cat-file -p` to work with trees and commits

The `cat-file` command already pretty-prints blob information. Now let's make
it pretty-print tree and commit information as well, just as `git` does.

Given the address of a stored tree object, `cat-file -p` should print each
entry therein as follows: `<mode> <type> <addr>\tab<name>\n`, where `<mode>` is
the entry's mode, `<type>` is the entry's type (either `tree` or `blob`),
`<addr>` is the entry's address, and `<name>` is the entry's name. For example,
given the final tree in the [Tree objects](#tree-objects) section, `git
cat-file -p 7bfe0b8771286dee5ce7cb50a9e85f6c4ddbd712` prints:

```
040000 tree cde220501bf8f8bb1e33dc49c3c0860e6f5a1132	dir
```

...and `git cat-file -p cde220501bf8f8bb1e33dc49c3c0860e6f5a1132` prints:

```
100644 blob d03e2425cf1c82616e12cb430c69aaa6cc08ff84	file
```

(Note the tab characters in both outputs above.)

Given the address of a stored commit object, `cat-file -p` should first print
the headers, one per line, followed by a blank line and then the message. Each
header should have the lowercase name of the header, a space, and the value,
terminated by a newline. The headers should be printed in the following order:
`tree`, `parent`, `author`, and `committer`. For an example, see the [commit
objects](#commit-objects) section above.


#### 7. Extend the help message output

First, the `help` command should be extended to include new or updated help
information for the `write-wtree`, `commit-tree`, and `cat-file` commands, as
detailed above.

Second, the top-level usage info should be printed under the same conditions as
before and should be as follows:

```
idiot: the other stupid content tracker

Usage: idiot [<top-args>] <command> [<args>]

Top-level arguments:
   -r <dir>   run from the given directory instead of the current one
   -d <dir>   store the database in <dir> (default: .idiot)

Commands:
   help
   init
   hash-object [-w] <file>
   cat-file {-p|-t} <address>
   write-wtree
   commit-tree <tree> -m "<message>" [(-p <parent>)...]
```


### Submission instructions

You will need to submit your code to Gradescope as a Clojure deps project. What
this means is that the top-level directory of the repository or .zip file that
you submit to Gradescope should have a `src` directory with at least an
`idiot.clj` file in it as well as a `deps.edn` file whose contents should be at
least `{}`. If your submission is missing these elements, or if they're not in
the top-level (root) directory, then the autograder will not work.

As of Thursday, February 27<sup>th</sup>, the autograder is available on
Gradescope.


### Hints

#### Automated tests

Because long feedback loops, such as those used by the autograder, are
frustrating, I recommend writing your own tests. I won't be providing the test
suite that the autograder uses this time, to make room for you to write your
own&mdash;but feel free to copy whatever you can from the autograder output.

#### Order of operations when storing trees

Trees need to store the address of each of their objects. To compute the
address for an object, you have to determine its contents, because git is a
_content-addressable_ database. For blobs, this is easy: just hash the contents
of the file, remembering to include the blob header. For trees, this is harder,
but it's exactly the problem that you're already solving to store the parent
tree.

In other words, you need to store all the objects for a given directory before
storing the directory itself. And to store sub-directories, just recur.

For example, if the only thing in the `-r` directory (other than the object
database directory) is `dir1/dir2/file` (i.e. a directory containing a
directory containing a file), the order of operations is like this:

1. store the contents of the `dir1/dir2/file` file as a blob
2. store the entries of the `dir1/dir2` directory as a tree, using the blob
   address computed in the previous step
3. store the entries of the `dir1` directory as a tree, using the tree address
   computed in the previous step
4. finally, store the entries of the root directory as a tree, using the tree
   address computed in the previous step

#### Difficulties with black-box testing for `commit-tree` command

It's not as easy to give good output from some of the autograder tests of the
`commit-tree` command because I'm just testing that an object file exists. So I
wanted to give the full test case for a complicated `commit-tree` test so that
you could test it locally. If you can make this test case pass locally, chances
are that you have a fully correct solution for the `commit-tree` command.

```clojure
(require '[clojure.java.io :as io])

(defn run-fast
  "Call the idiot program in the current process with the given string of args."
  [cmd]
  (->> (str/split cmd #"\s+")
       (remove str/blank?)
       (apply sut/-main)
       with-out-str))

(def tmp-dir "spec-run-tmp")

(defn run-in-tmp-dir [cmd]
  (run (format "-r %s %s" tmp-dir cmd)))

(describe "The commit-tree subprogram"
  (context "in a repo context"
    (before
      (.mkdir (io/file tmp-dir))
      (run-in-tmp-dir "init"))
    (after
      (rm-rf (io/file tmp-dir)))

    (it "handles 8 parents (octopus merge!)"
      (let [data "contents\n"
            addr "8add0d07efc6ba027407c82740a001cfcbc7b772"
            commit1-addr "3d28f2e32cb7c896580a8dc3ab776d5211c3985b"
            commit2-addr "d17cb05667daab794ef922502f169a99d0af94a4"
            commit3-addr "8e3c418d416205d9133a8a0055cf9df9581eae6d"
            commit4-addr "9c709af31f8dbbd0020e9e9e65d1dfc3638e6404"
            commit5-addr "f6212a0b484f3a3c42db1edd11159d0b7f9ddeed"
            commit6-addr "2a30f8658fd6bfd923d81a7460e28692cfc674e1"
            commit7-addr "019fb70a39d1684ab421b6b4be01a03a3af5d134"
            commit8-addr "f1e5402875bb52a22910229f5474386eb2dfe3ca"
            octopus-addr "695d14ba9e1bddea8e423d61c5b58ec4e956618c"
            first2 (subs octopus-addr 0 2)
            last38 (subs octopus-addr 2)
            obj-path (format "%s/.idiot/objects/%s/%s" tmp-dir first2 last38)
            run* (fn [msg parent-addrs]
                   (let [-p-options (map #(str " -p " %) parent-addrs)
                         -p-str (apply str -p-options)
                         cmd (format "commit-tree %s -m %s%s"
                                     addr msg -p-str)]
                     (run-in-tmp-dir cmd)))]

        (spit (str tmp-dir "/file") data)
        (run-in-tmp-dir "write-wtree")
        (run* "msg" [])
        (run* "msg2" [commit1-addr])
        (run* "msg3" [commit2-addr])
        (run* "msg4" [commit3-addr])
        (run* "msg5" [commit4-addr])
        (run* "msg6" [commit5-addr])
        (run* "msg7" [commit6-addr])
        (run* "msg8" [commit7-addr])
        (run* "octopus" [commit1-addr commit2-addr commit3-addr commit4-addr
                         commit5-addr commit6-addr commit7-addr commit8-addr])
        (should (-> obj-path io/file .exists))))))
```

Note that you can run the equivalent commands with git and get the exact same
addresses. Just remember to set the environment variables (like
`GIT_AUTHOR_NAME`) as done above so that the contents will line up exactly.

#### Dealing with binary data

_(An earlier version of this section had more code but was less organized. You
can still see it [on the repo for this site
here](https://gitlab.com/jeff.terrell/comp590-software-arch-website/-/blob/661874ab0b5d252dfc9d976e81adfd57d2027785/pages/idiot-commit.md).)_

The code I provided last time for computing git addresses and reading files
from disk will not work with trees. Trees have binary data in them, and
treating that binary data as a string, as the earlier code does, will interpret
the bytes as unicode data rather than raw bytes. (See [this classic
post](https://www.joelonsoftware.com/2003/10/08/the-absolute-minimum-every-software-developer-absolutely-positively-must-know-about-unicode-and-character-sets-no-excuses/)
for more details on unicode.)

In a nutshell, the key observation is that a single unicode character can take
more than one byte, depending on the particular byte sequence. So a sequence of
20 bytes, when interpreted as a unicode string, might be e.g. 11 characters
long.

Instead, any function which might sometimes deal with binary data should always
accept and return a sequence of bytes rather than a string, because the byte
sequence is the lowest common denominator. If the caller knows it's dealing
with strings, it can always cast to a string after calling the lower-level
function.

Here's a menu of things related to binary data.

- Get the SHA1 address of a byte sequence, as a byte sequence: see the
  `sha-bytes` function in the [Storage details](#storage-details) section
  above.
- Convert byte seq into a hex string: see the `to-hex-string` function above.
- Convert a hex string to a byte sequence

    ```clojure
    (defn hex-digits->byte
      [[dig1 dig2]]
      ;; This is tricky because something like "ab" is "out of range" for a
      ;; Byte, because Bytes are signed and can only be between -128 and 127
      ;; (inclusive). So we have to temporarily use an int to give us the room
      ;; we need, then adjust the value if needed to get it in the range for a
      ;; byte, and finally cast to a byte.
      (let [i (Integer/parseInt (str dig1 dig2) 16)
            byte-ready-int (if (< Byte/MAX_VALUE i)
                             (byte (- i 256))
                             i)]
        (byte byte-ready-int)))

    (defn from-hex-string
      [hex-str]
      (byte-array (map hex-digits->byte (partition 2 hex-str))))
    ```

- Combine two byte sequences: just use the built-in `concat` function.
- Casting a string to a byte sequence: just use the `.getBytes` method on the
  string, e.g. `(.getBytes a-string)`.
- Read a seq of unzipped bytes from a zipped file on disk:

    ```clojure
    (defn unzip
      "Unzip the given file's contents with zlib."
      [path]
      (with-open [input (-> path io/file io/input-stream)
                  unzipper (InflaterInputStream. input)
                  out (ByteArrayOutputStream.)]
        (io/copy unzipper out)
        (.toByteArray out)))
    ```

- Casting a byte sequence to a string:

    ```clojure
    ;; Note that if given binary data this will fail with an error message
    ;; like:
    ;; Execution error (IllegalArgumentException) at ,,,.
    ;; Value out of range for char: -48
    (defn bytes->str [bytes]
      (->> bytes (map char) (apply str)))
    ```

- Split a byte sequence at a certain value, e.g. 0:

    ```clojure
    (defn split-at-byte [b bytes]
      (let [part1 (take-while (partial not= b) bytes)
            part2 (nthrest bytes (-> part1 count inc))]
        [part1 part2]))
    ```

#### More visibility on what the specs are doing

Sometimes you might get a failure message that isn't very helpful to show what
the issue might be. This is frustrating. I'd like to give you better output,
but that's not easy to do, for various reasons. And I don't want to merely
provide the entire test suite, because then you're not learning how to write
your own tests. (You are writing your own tests, aren't you?)

As a compromise, what follows is a description of what the tests are doing, for
each test that doesn't provide helpful error messages.

The following shorthand applies to what follows:

- `idiot` means executing `clojure -m idiot` with whatever arguments follow
- "run" means running idiot
- "run\*" means running idiot with a `-r spec-run-tmp` switch, where the
  directory exists, and a repository has been initialized with `idiot -r
  spec-run-tmp init` before running other commands.

Here are the details:

- `The program respects the -r switch to choose a different working directory`
    - run\* `init`
    - assert that `spec-run-tmp/.idiot` directory exists
    - assert that `.idiot` directory does not exist

- `The program respects the -d switch to choose a different database directory`
    - run\* `-d .idiot2 init`
    - assert that `spec-run-tmp/.idiot2` directory exists
    - assert that `spec-run-tmp/.idiot` directory does not exist

- `The init subprogram still detects various error conditions`
    - run\* `init database`
    - assert output is `"Error: init accepts no arguments\n"`
    - mkdir `spec-run-tmp/.idiot`
    - run\* `init`
    - assert output matches `#"Error: .* directory already exists"`

- `The init subprogram still initializes a database and informs the user`
    - run\* `init`
    - assert output matches `#"Initialized empty Idiot repository in .* directory\n"`
    - assert that `spec-run-tmp/.idiot` is a directory
    - assert that `spec-run-tmp/.idiot/objects` is a directory

- `The hash-object subprogram still prints the correct address when no -w switch is given`
    - the contents of the tmp-file are `"hello\n"`

- `The hash-object subprogram still prints the address and writes the blob with the -w switch`
    - the contents of the tmp-file are `"something\n"`
    - assert that the output of running\* `hash-object -w <tmp-file>` is the
      correct address
    - assert that `spec-run-tmp/.idiot/objects/<first2>` is a directory
    - assert that `spec-run-tmp/.idiot/objects/<first2>/<last38>` is a file
    - assert that the unzipped contents are correct

- `The write-wtree subprogram in a repo context writes no objects if no files/dirs exist (besides database dir)`
    - assert that the output of running `write-wtree` is `"writes no objects if
      no files/dirs exist (besides database dir)"`
    - assert that the directory `spec-run-tmp/.idiot/objects` is empty

- `The write-wtree subprogram in a repo context writes a blob object for each file in top-level repo dir`
    - spit `"something\n"` to file `foo` and `"something else\n"` to file `bar`
    - assert that objects exist at the corresponding addresses in the
      `spec-run-tmp/.idiot` directory

- `The write-wtree subprogram in a repo context skips overwriting a blob object that already exists`
    - compute the address for the content `"hello there\n"`
    - spit `"blah blah blah"` to the object for that address in the
      `spec-run-tmp/.idiot` repo directory
    - spit `"hello there\n"` to file `spec-run-tmp/foo`
    - run\* write-tree
    - slurp the object file and assert its contents are still `"blah blah blah"`

- `The write-wtree subprogram in a repo context writes a tree object containing the files in the repo dir & prints addr`
    - spit `"star\n"` to `spec-run-tmp/foo` and `"wars\n"` to `spec-run-tmp/bar`
    - run\* write-wtree and assert output address is correct
    - read and unzip the data from the tree object file and assert that its
      contents are correct

- `The write-wtree subprogram in a repo context writes a tree object for each dir in the repo dir`
    - mkdir spec-run-tmp/dir
    - spit `"tarheel\n"` to `spec-run-tmp/dir/file`
    - run\* write-wtree
    - assert that the root tree exists in the right place under
      `spec-run-tmp/.idiot/objects`
    - assert that the contents of the root tree are correct
    - assert that the `dir` tree exists in the right place under
      `spec-run-tmp/.idiot/objects`
    - assert that the contents of the `dir` tree are correct

- `The write-wtree subprogram in a repo context writes nested tree objects corresponding to nested dirs`
    - make directories for `spec-run-tmp/dir1/dir2`
    - spit `"go heels go!\n"` to `spec-run-tmp/dir1/dir2/file`
    - mimic the approach of the other test, but include the extra tree

- `The write-wtree subprogram in a repo context writes files nested in subdirectories`
    - spit `"tarheel\n"` to `spec-run-tmp/dir/file`, making the directory first
    - run\* `write-wtree`
    - assert that the unzipped contents of the blob object file are correct

- `The write-wtree subprogram in a repo context skips empty directories`
    - mkdir `spec-run-tmp/empty-dir`
    - run\* `write-wtree`
    - assert that the `spec-run-tmp/.idiot/objects` directory is empty

- For the `commit-tree` tests, see the hint above with the octopus merge. If
  you can get that working, you will probably pass all the other tests.

- The `cat-file` tests are pretty straightforward and easy to mimic locally.


#### Windows path separators

If your program runs correctly on your Windows machine but doesn't pass the
autograder, it might be because of the difference between path separators. In
Windows, backslashes (`\`) separate path elements. In Linux (and MacOS), it's
forward slashes (`/`). The autograder runs Linux.  So, if your program only
works with backslashes and not with slashes, the autograder will not give you
points.

One reason you might do this is to compute the repo database directory. For
example, say the `-r` switch is `tmp` and `-d` isn't specified so defaults to
`.idiot`, and say that names `r` and `d` have been bound to the respective
values. Then code like this **will not work in the autograder**:

- `(str r "\\" d "\\" "objects")`
- `(format "%s\\%s\\objects" r d)`
- `(str/join "\\" [r d "objects"])`

(Note that in a Clojure or Java string, two backslashes are needed to create a
single backslash character.)

Instead, you can use a static string in `java.io.File` to get the
platform-specific separator that the current OS expects. If you've already
imported `java.io.File` (e.g. in your `ns` form), then `File/separator` will be
either `/` or `\`, depending.

You can also use one of the `File` constructors that accepts both parent and
child arguments and joins them using the separator. There's [one that takes a
File
parent](https://docs.oracle.com/javase/10/docs/api/java/io/File.html#%3Cinit%3E(java.io.File,java.lang.String))
and [one that takes a String
parent](https://docs.oracle.com/javase/10/docs/api/java/io/File.html#%3Cinit%3E(java.lang.String,java.lang.String)),
and both take a String child. For example, `(io/file r d)` works to join `r`
and `d` regardless of whether `r` is a String or File. In fact, the `io/file`
function accepts arbitrarily many arguments and joins them all together. For
for example, `(io/file r d "objects")` joins `r`, `d`, and `"objects"` to
create `tmp/.idiot/objects` on Linux or `tmp\.idiot\objects` on Windows.

